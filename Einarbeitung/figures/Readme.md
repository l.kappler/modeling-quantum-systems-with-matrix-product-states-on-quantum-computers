## RDM:
Data used for plots is stored in the json files with the same name. They are created by jsonpickle in python via `jsonpickle.encode(data)` and can be read via `data = jsonpickle.decode(json_file.read())`
