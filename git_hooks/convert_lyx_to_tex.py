#!/home/leo/Daten/.miniconda3/envs/gitpython/bin/python

# should change so that the staged version of lyx file is used. see https://stackoverflow.com/questions/36429482/gitpython-how-can-i-access-the-contents-of-a-file-in-a-commit-in-gitpython
# maybe do some of this with a make file so that it only runs what is acutally needed? maype stash the wd and get back the index into wd. then run the makefile? but that might alter the timestamps in a way that break make?

print("running Thesis compile script.")
import os
import subprocess
import shutil
from itertools import chain
try:
    import git
except ImportError:
    print("you need to install the library gitpython and choose the python executable from this env in the scripts Shebang")



tex_files_dir = "Thesis/Uni Vorlage/chapters/"

# Specify the directory to include files from
lyx_files_dir = "Thesis/"
latex_Parent_files_dir = "Thesis/Uni Vorlage/"
latex_doc_name = "bthesis"

# Initialize repository
repo = git.Repo(os.getcwd())

# Get list of staged files
staged_files = repo.head.commit.diff(git.IndexFile.Index)

something_changed = False

for file in chain(staged_files.iter_change_type('A'),
                            staged_files.iter_change_type('M')):
    if file.a_path.startswith(lyx_files_dir) and file.a_path.endswith('.lyx'):
        something_changed = True
        try:
            # Run lyx command on file
            print(f"recreate .tex chapter for {file.a_path}")
            subprocess.run(['lyx', '-e', 'pdflatex', file.a_path], check=True)

            # Get name of .tex file
            tex_file = file.a_path.replace('.lyx', '.tex')
            new_tex_file_loc = os.path.join(tex_files_dir, os.path.basename(tex_file))

            # Move .tex file to new directory
            shutil.move(tex_file, new_tex_file_loc)

            # Add .tex file in new directory to staging area
            repo.git.add(new_tex_file_loc)
        except subprocess.CalledProcessError:
            print(f"Error: Failed to run lyx command on {file.a_path}. Commit aborted.")
            exit(1)
if something_changed:
    # lyx creates macros_for_lyx.tex when ever it creates the .tex file for a lyx-file that inputs macros_for_lyx.lyx.
    # lets remove it.
    try:
        os.remove(lyx_files_dir + "macros_for_lyx.tex")
    except FileNotFoundError:
        # the file might not exist because macros_for_lyx.lyx is itself in the index and was created last,
        # which means that macros_for_lyx.tex was overwritten and then moved to chapters/
        if lyx_files_dir + "macros_for_lyx.lyx" not in map(lambda file_obj: file_obj.a_path, staged_files):
            print("soemthing is weird. look at the python code for converting lyx files!")
            raise

    print("recreating pdf")
    with open(latex_Parent_files_dir + latex_doc_name + ".log", "w") as log_file:
        subprocess.run(["pdflatex", latex_doc_name], cwd=latex_Parent_files_dir, stdout=log_file, stderr=log_file, check=True)
        subprocess.run(["biber", latex_doc_name],    cwd=latex_Parent_files_dir, stdout=log_file, stderr=log_file, check=True)
        subprocess.run(["pdflatex", latex_doc_name], cwd=latex_Parent_files_dir, stdout=log_file, stderr=log_file, check=True)
        subprocess.run(["biber", latex_doc_name],    cwd=latex_Parent_files_dir, stdout=log_file, stderr=log_file, check=True)
        subprocess.run(["pdflatex", latex_doc_name], cwd=latex_Parent_files_dir, stdout=log_file, stderr=log_file, check=True)
    repo.git.add(latex_Parent_files_dir + latex_doc_name + ".tex")
    repo.git.add(latex_Parent_files_dir + latex_doc_name + ".pdf")
