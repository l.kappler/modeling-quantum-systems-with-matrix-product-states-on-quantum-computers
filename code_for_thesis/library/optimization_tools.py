"""
This library contains the rotosolve algorithm and all double sinusoidal related functions.
"""

import numpy as np
import scipy
import pychebfun

import functools
from tqdm import tqdm
import sys

from .tools import compare_functions
from .settings import settings


def find_global_min(fun, derivative, args, domain=(0,2*np.pi)):
    """
    approximate a function with polynomials and find the minimums
    """
    f_cheb = pychebfun.Chebfun.from_function(lambda theta: derivative(theta, *args), domain=domain)
    potential_minimizers = np.r_[0, f_cheb.roots()]
    x=np.linspace(*domain, 100)
    #display(plt.plot(x, fun(x, *args)))
    #print(potential_minimizers)
    return potential_minimizers[np.argmin([fun(theta, *args) for theta in potential_minimizers])]


def minimize_with_bfgs(cost_func, init_params, desired_minimum=None, tol=1e-10, gtol=None, context=None):
    """
    accepts list and dict as init_params.
    returns the same type of params.

    if desired_minimum is not None check if the final function value is in the range tol around desired_minimum
    if not print a warning to stderr. you can pass context as a string that will be printed along the warning.

    gtol: gradient tolerance for bfgs.
    """
    if context:
        context = f" context: {context}"
    keys = None
    if type(init_params) == dict:
        keys = init_params.keys()
        init_params = list(init_params.values())
        cf = lambda params: cost_func(dict(zip(keys, params)))
    else:
        cf = cost_func

    result = scipy.optimize.minimize(cf, init_params, method="BFGS", tol=gtol)

    # check the output
    if not result.success:
        print(f"========warning: BFGS did not converge.{context}", file=sys.stderr)
        print(result, file=sys.stderr)
    if not np.abs(result.fun - desired_minimum) < tol:
        print(f"========warning: desired_minimum: {desired_minimum} not reached: end value is {result.fun}{context}", file=sys.stderr)

    result = result.x
    if keys is not None:
        result = dict(zip(keys, result))
    return result

def minimize(cost_func, init_params, desired_minimum=None, tol=1e-10, gtol=None, context=None, is_noisy=True, debugging=False):
    """
    accepts list and dict as init_params.
    returns the same type of params.

    if desired_minimum is not None check if the final function value is in the range tol around desired_minimum
    if not print a warning to stderr. you can pass context as a string that will be printed along the warning.

    gtol: gradient tolerance for bfgs or other tol for optmization algorithm.
    is_noisy: if the function is noisy choose the other optimization algorithm defined in settings
    """
    if context:
        context = f" context: {context}"
    keys = None
    if type(init_params) == dict:
        keys = init_params.keys()
        init_params = list(init_params.values())
        cf = lambda params: cost_func(dict(zip(keys, params)))
    else:
        cf = cost_func

    method = settings["optimization algorithms"]["is noisy" if is_noisy else "not noisy"]
    if debugging: print(f"using optimizer method {method}")

    offset = np.zeros(len(init_params))
    init_params = np.array(init_params)
    for i in range(10):
        result = scipy.optimize.minimize(cf, init_params+offset, method=method, tol=gtol)
        if result.fun -desired_minimum < tol:
            break
        else:
            offset = np.random.random(len(init_params))/10


    # check the output
    if not result.success:
        print(f"========warning: {method} did not converge.{context}", file=sys.stderr)
        print(result, file=sys.stderr)
    if not np.abs(result.fun - desired_minimum) < tol:
        print(f"========warning: desired_minimum: {desired_minimum} not reached: end value is {result.fun}, method used: {method}.{context}", file=sys.stderr)

    result = result.x
    if keys is not None:
        result = dict(zip(keys, result))
    return result



# double sinusoidal stuff
def double_sinosoid_template(theta, P, phi, Q, psi):
    return P * np.sin(2*theta + phi) + Q * np.sin(theta + psi)

def double_sinosoid_deriv(theta, P, phi, Q, psi):
    return 2 * P * np.cos(2*theta + phi) + Q * np.cos(theta + psi)


def check_double_sinusoidality(func):
    args = get_double_sinusoidal_args(func)
    print(f"if sinusoidal, the args are: {args}")
    if compare_functions(func, lambda theta: double_sinosoid_template(theta, *args)):
        print("func is sinusoidal.")


def get_double_sinusoidal_args(double_sinusoidal_fun):
    # if the function is costly to evaluate use functools.lru_cache
    # to reduce calls for double_sionusoidal_fun
    results = double_sinusoidal_fun((0, np.pi, np.pi/2, -np.pi/2, np.pi/4, -np.pi/4))
    A = (results[0] + results[1])
    B = (results[0] - results[1])
    C = (results[2] + results[3])
    D = (results[2] - results[3])
    E = (results[4] - results[5])

    a, b, c, d = 1/4*(2*E-np.sqrt(2)*D), 1/4*(A-C), 1/2*D, 1/2*B

    P = np.sqrt(a**2+b**2)
    phi = np.arctan2(b, a)

    Q = np.sqrt(c**2+d**2)
    psi = np.arctan2(d, c)
    return (P,phi,Q,psi)


# double rotosolve
def double_rotosolve(double_sinusoidal_fun, inital_params, max_iterations=25, tolerance=1e-10, desired_minimum=None, quiet=False, context="", log_file=None, may_overwrite=False):
    """
    bad habbits of this algorithm:
    - order of parameter keys matters. Therefore the names of the parameters matter.
    - stops optimizing once it found a global minimum when only looking in each axis direction. If the optimal solution is on a diagonal it won't find it.

    if you know the function value at the minimum, pass it to the function so that it knows when it didn't converge yet and can give a warning if it doesn't work.
    use tollerance to give the range in which the function may be at the end.

    quiet: if quiet the function won't print status and current params but still warnings.
    context: optional string to print together with warnings and with the the status if not quiet.
    """
    if not quiet and context != "":
        print(f"context for the following: {context}")
    if context != "":
        context = f"\ncontext: {context}"
    params = inital_params
    last_func_val = double_sinusoidal_fun((params,))[0]
    if log_file:
        f = open(log_file, "w" if may_overwrite else "x")
        f.write("function value,")
        f.write(",".join([str(key) for key in params.keys()]) + "\n")
        f.write(f"{last_func_val},{','.join([str(value) for value in params.values()])}\n")

    try:
        for iteration in range(max_iterations):
            change = 0
            for key in reversed(params.keys()):

                @functools.lru_cache
                def M(theta_list):
                    if type(theta_list) != tuple:
                        theta_list = (theta_list,)
                    # wrapper for caching the function and using the params except for the one corresponding to key.
                    res = double_sinusoidal_fun([{**params, key:theta} for theta in theta_list])
                    #print(f"called M for theta: {theta} and key: {key}, res: {res}")
                    return res

                args = get_double_sinusoidal_args(M)
                P,phi,Q,psi = args

                if not quiet: print(f"current params: {params}\nsinusoidal args: {args}")
                #compare_functions(M, lambda theta: double_sinosoid_template(theta, P,phi,Q,psi))
                if abs(P) + abs(Q) > tolerance:
                    new_theta = find_global_min(double_sinosoid_template, double_sinosoid_deriv, args)
                    change += abs(params[key]-new_theta)
                    if M(new_theta) > last_func_val:
                        print(f"bad accuracy. new func value: {M(new_theta)}, previous was {last_func_val}. The old {key} was possibly better.", file=sys.stderr)
                        print("improve your cost function accuracy, check that it is doubly sinusoidal or try larger tollerance.", file=sys.stderr)
                        #new_theta = params[key]
                    else:
                        params[key] = new_theta
                        if not quiet: print(f"new {key}: {new_theta}")
                else:
                    if not quiet: print("skipping because function is constant in the parameter's direction.")
                if log_file: f.write(f"{M(new_theta)[0]},{','.join([str(value) for value in params.values()])}\n")
                if not quiet: print("current value:", M(new_theta))
                if desired_minimum is not None and M(new_theta) < desired_minimum + tolerance:
                    return params
                last_func_val = M(new_theta)
            if change < tolerance:
                break
    finally:
        if log_file: f.close()
    # if we got here than the function did not return because we reached the desired_minimum.
    # so if there is a desired_minimum, print a warning.
    if iteration == max_iterations-1 and not change < tolerance:
        print(f"reached max iter: {max_iterations}.{context}", file=sys.stderr)
    if desired_minimum is not None:
        print(f"========warning: desired_minimum: {desired_minimum} not reached, fun is at {double_sinusoidal_fun((params,))[0]}.{context}", file=sys.stderr)

    return params
