# measurements
from qiskit.primitives import Estimator, Sampler
from qiskit.quantum_info import SparsePauliOp
from qiskit import Aer, transpile, QuantumCircuit

import numpy as np
import scipy

from .tools import relu, save_job, log_job_if_wanted
from .settings import settings, _shared_variables

# simulations
estimator = Estimator(options=settings["estimator_options"])
aer_sim = Aer.get_backend('statevector_simulator')

def simulate_circ(circ):
    traspiled_circ = transpile(circ, backend=aer_sim)
    job = aer_sim.run(traspiled_circ)
    result = job.result()
    return result.get_statevector(circ, decimals=10), result



# observables

is_1_measure = SparsePauliOp.from_list([("Z",-.5), ("I", .5)])
is_0_measure = (SparsePauliOp("I") - is_1_measure).simplify()

def create_obs_from_string(description):
    """
    string may only contain the chars i, 0, 1, x, y, z (case insensitive)

    !!! the order of qubits is opposite to what we see in textbooks.
    see https://qiskit.org/documentation/explanation/endianness.html
    Begin the observable with the most important bit (MIB)
    That is the qubit with the highest index i.e. the one at the bottom of the circuit diagram.

    Returns:
        Observable as a tensor product of SparsePauliOp objects with the following mapping:
        I -> Id
        0 -> diagonal in computational basis eigenvalue 1 for |0> and 1 for |1>
        1 -> diagonal in computational basis eigenvalue 1 for |1> and 0 for |0>
        x,y,z -> corresponding Pauli Operator
    """
    obs = SparsePauliOp("")
    for char in description.upper():
        match char:
            case "0":
                # the wedge ^ denotes tensor product for SparsePauliOp.
                obs ^= is_0_measure
            case "1":
                obs ^= is_1_measure
            case c if c in "IXYZ":
                obs ^= SparsePauliOp(char)
            case _:
                raise ValueError(f"Observable description may only contain combinations of the characters 'I01XYZ'. But description contains '{char}'.")
    return obs



# measure functions
def measure_0_strings(N_qubits):
    return create_obs_from_string("0"*N_qubits)

def estimate_0_strings_prob(circ_list, **run_kwargs):
    if type(circ_list) not in [list, tuple]:
        circ_list = [circ_list]
    N_list = [circ_.num_qubits for circ_ in circ_list]
    if settings["simulated"]:
        obs_list = [measure_0_strings(N_) for N_ in N_list]

        # in the current qiskit version the estimator seems to check if it already estimated the circuit and if it did,
        # return the cached result. However checking if the circuit is the same as an earlier one, doesn't work properly
        # if it uses custome gates like R, L, U etc. and only thoes change. Therefore recreate the Estimator object.
        estimator = Estimator(options={'shots':None, "approximation":True})
        job = estimator.run(circ_list, obs_list, **run_kwargs)
        if np.any(job.result().values[:]<0):
            print("negative value in estimate 0 strings!!! This value can't be negative:", job.result().values[:])
        return relu(job.result().values[:]) # if negative set it 0. theoretically it can't be 0. must be rounding errors.
    else:
        assert False, "needs reimplementation with support for circ lists."
        available_qubits = settings["backend"].configuration().n_qubits
        available_qubits = 20
        number_circuits = available_qubits//N
        qc = QuantumCircuit(number_circuits*N)

        for i in range(number_circuits):
            qc.compose(circ, range(i*N, (i+1)*N), inplace=True)
        qc.measure_all()
        runs_on_qc = np.ceil(settings["min shots"]/number_circuits)
        shots = runs_on_qc*number_circuits

        transpiled_circuit = transpile(qc, settings["backend"], optimization_level=3)

        #return qc, runs_on_qc, shots
        sampler = Sampler(options={"shots": runs_on_qc, "resilience_level":2}, backend=settings["backend"])
        job = sampler.run(transpiled_circuit, **run_kwargs)

        save_job(job)
        return get_zero_string_prob_from_sampler_job(job, number_circuits, N)

def get_zero_string_prob_from_sampler_job(job, number_circuits, N):
    zero_string_counts = 0
    results = job.result().quasi_dists
    assert len(results) == 1, "result length >1 not implemented."

    counts = results[0]
    for key in counts.keys():
        result = key
        print("k", key)
        for i in range(number_circuits):
            if result % 2**N == 0:
                zero_string_counts += counts[key]
            print(result)
            result >>= N
    return zero_string_counts/number_circuits

def estimate_0_string_amplitude_and_add_up(circuits, params, **run_kwargs):
    """
    ciruits is a list of (circuit, factor) pairs.

    Returns: factor * sqrt(expectation) of zero_strings after circuit.
    """
    result = np.zeros((len(params),))
    for (circuit, factor) in circuits:
        relevant_params = [{key: params_[key] for key in circuit.parameters} for params_ in params]
        circuits = [circuit.bind_parameters(relevant_params_) for relevant_params_ in relevant_params]
        result += factor * np.sqrt(estimate_0_strings_prob(circuits, **run_kwargs))
    return result

# sometimes I have to redefine the estimator here because it caches the results from previous runs
# which leads to >20GB of RAM usage when optimizing for a ground state.

def set_estimator(Estimator_class, **kwargs):
    _shared_variables["estimator"] = Estimator_class(**kwargs)
    settings["delete estimator after n estimations"] = float("inf")

estimator_count = 0
def estimate(*args, context, **kwargs):
    global estimator_count
    estimator_count += 1
    if estimator_count > settings["delete estimator after n estimations"]:
        _shared_variables["estimator"] = Estimator(options=settings["estimator_options"])
        estimator_count = 0
    job = _shared_variables["estimator"].run(*args, **kwargs)
    log_job_if_wanted(job, context, _shared_variables["estimator"].options)
    return job.result()
