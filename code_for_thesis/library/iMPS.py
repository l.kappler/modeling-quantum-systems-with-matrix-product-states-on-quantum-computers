import numpy as np
import tensornetwork as tn

from qiskit import QuantumCircuit
from qiskit.circuit.library import TwoLocal

from .measurement_tools import estimator, create_obs_from_string
from .tools import get_matrix_for_gate, plot_tensor_network, get_params, join_params, complex_conjugate_instruction
from .evironment_V import find_environment_V

def create_U_gate(name, bond_dim, num_init_qubits, reps=2, **twoLocal_kwargs):
    parameter_prefix=name.lower()
    return TwoLocal(bond_dim+num_init_qubits, ['rz', 'rx'], 'cx', 'linear',reps=reps, name=name, parameter_prefix=parameter_prefix, skip_final_rotation_layer=True, **twoLocal_kwargs)


class _iMPS(object):
    # cache for variables
    _V_params = None

    @property
    def bond_dim(self):
        return self.V.num_qubits//2

    @property
    def initialized_qubits_per_U(self):
        return self.U.num_qubits - self.bond_dim

    @property
    def V_param_n(self):
        """V parameter names as a list"""
        return get_params(self.V)

    @property
    def U_param_n(self):
        """U parameter names as a list"""
        return get_params(self.U)

    @property
    def param_n(self):
        """all parameter names as a list"""
        return self.V_param_n + self.U_param_n

    @property
    def V_params(self, **optimization_kwargs):
        """
        V parameter dictionary
        This dictionary is automatically created on the first call since set_U_params was called.
        """
        if self._V_params is not None:
            return self._V_params

        self._V_params = find_environment_V(self.U, self.V, self.V_init_params, self.U_params, **self.V_optimization_kwargs)
        return self._V_params

    @property
    def U_params(self):
        """U parameter dictionary"""
        return self._U_params

    @property
    def params(self):
        """V parameter dictionary"""
        return join_params(self.V_params, self.U_params)

    @property
    def U_conj(self):
        return complex_conjugate_instruction(self.U)


    def __init__(self, V, U, U_params):
        """
        V, U: qiskit Instructions
        U_params: list or dict with corresponding parameters.

        The parameters for V are automatically found when needed such that the iMPS is translation invariant.
        you can overwrite the initial parameters at V_init_params

        V is assumed to have double the number of qubits of log2(bond_dim)
        only like this we can reach any density matrix. might want to relax that condition for approximations.

        overwriting V and U after initialization not nessecarily supported.
        """

        assert V.num_qubits % 2 == 0, f"V is assumed to act on double the number of qubits of log2(bond_dim) but acts on {V.num_qubits} qubits"

        self.V = V
        self.U = U

        self.set_U_params(U_params)
        np.random.seed(42)
        self.V_init_params = dict(zip(self.V_param_n, np.random.random(len(self.V_param_n))))
        self.V_optimization_kwargs = {"quiet": True, "context": f"find {self.V.name} environment params."}

    def set_U_params(self, U_params):
        if type(U_params) == dict:
            assert U_params.keys() == self.U_param_n, f"the keys of the new parameter dict aren't the Parameters needed bei U. Needed:\n{list(self.U_param_n)},\nyou provided:\n{list(U_params.keys())}"
        else:
            assert len(U_params) == len(self.U_param_n), f"you must provide as many parameters as U needs. You provided {len(U_params)}, needed: {len(self.U_param_n)}"
            U_params = dict(zip(self.U_param_n, U_params))
        self._update_U_params_hook()
        self._U_params = U_params

    def _update_U_params_hook(self):
        # reset properties that might change and where cached.
        self._V_params = None

class CiMPS(_iMPS):
    """
    classical iMPS (matrices are still given as qiskit instructions but we always only use the matrix rep.)
    """

    # cache storage:
    _U_mat = None
    _Uinv_mat = None

    @property
    def U_mat(self, precission=20):
        if self._U_mat is not None and self._U_mat_precission >= precission: return self._U_mat
        self._U_mat = get_matrix_for_gate(self.U, self.U_params, precission)
        self._U_mat_precission = precission
        return self._U_mat

    @property
    def Uinv_mat(self, precission=20):
        if self._Uinv_mat is not None and self._Uinv_mat_precission >= precission: return self._Uinv_mat
        self._Uinv_mat = get_matrix_for_gate(self.U.inverse(), self.U_params, precission)
        self._Uinv_mat_precission = precission
        return self._Uinv_mat

    def _update_U_params_hook(self):
        # reset properties that might change and where cached.
        super()._update_U_params_hook()
        self._U_mat = None
        self._Uinv_mat = None

class QiMPS(_iMPS):
    def construct_mps_ket_circ(self, num_needed_init_qubits):
        V = self.V
        U = self.U

        circ = QuantumCircuit(num_needed_init_qubits + 2*self.bond_dim)
        circ.append(V, range(V.num_qubits))

        num_init_qubits = 0
        while num_init_qubits < num_needed_init_qubits:
            offset = self.bond_dim + num_init_qubits
            circ.append(U, range(offset, U.num_qubits + offset))
            num_init_qubits += self.initialized_qubits_per_U
        #display(circ.draw())
        return circ

    def measure_observable(self, obs):
        num_needed_init_qubits = obs.num_qubits

        assert num_needed_init_qubits % self.initialized_qubits_per_U == 0, f"Each U gate initializes {self.initialized_qubits_per_U} qubits. The observable must measure on a multiple of that."

        circ = self.construct_mps_ket_circ(num_needed_init_qubits)

        # observable
        bond_dim_Id = create_obs_from_string("I"*self.bond_dim)
        obs = bond_dim_Id ^ obs ^ bond_dim_Id
        return estimator.run(circ.bind_parameters(self.params), obs)

class iMPS(CiMPS, QiMPS):
    pass

def get_transfer_matrix(ketMPS: CiMPS, braMPS: CiMPS, W, trotterization_order=1, plot_network=False):
    """
    returns: matrix of dim ( (2**bond_dim)**2, (2**bond_dim)**2 )
    """
    bond_dim = ketMPS.bond_dim
    iqpU = ketMPS.initialized_qubits_per_U
    assert bond_dim == braMPS.bond_dim, "bond dimensions must be equal for both mps to get the transfer matrix"
    assert iqpU == 1, " only initialized_qubits_per_U == 1 supported."
    assert iqpU == braMPS.initialized_qubits_per_U, "U matrices for both mps must have same dim to get the transfer matrix"
    if iqpU != 1:
        NotImplementedError("initialized_qubits_per_U may only be 1.")

    U_ket_edge_dims = 2**np.array((iqpU, bond_dim, bond_dim, iqpU))
    U_bra_edge_dims = 2**np.array((bond_dim, iqpU, iqpU, bond_dim))

    # get the matrices, for mps matrices already just use relevant parts.
    # we get the relevant parts by choosing the first element from the first axis
    # because this is equivalent to applying the matrix on a state where the
    # iqpU most important bits are in state |0>. (The most important bits are the lower ones in qiskit)
    Umat = ketMPS.U_mat.to_matrix().T.reshape(U_ket_edge_dims)[0]
    U_bra_mat = braMPS.Uinv_mat.to_matrix().T.reshape(U_bra_edge_dims)[...,0,:]

    match trotterization_order:
        case 1:
            assert W.num_qubits % iqpU == 0, f"W must act on a multiple of initialized_qubits_per_U={iqpU} qubits. It acts on {W.num_qubits} qubits."
            Us_needed = W.num_qubits//iqpU
            Wmat = get_matrix_for_gate(W, precission=20).to_matrix().T.reshape(2**np.array([iqpU]*Us_needed*2))
        case _:
            NotImplementedError("only trotterization_order=1 implemented.")

    Wnode = tn.Node(Wmat, name="W")
    last_U_node = tn.Node(Umat, name="U0")
    last_U_bra_node = tn.Node(U_bra_mat, name="U_bra0")
    outward_dangling_edges = [last_U_bra_node[2], last_U_node[0]]

    # when defining contractions of the tensor network i order the nodes in the way they where ordered on the circuit.
    last_U_node[2] ^ Wnode[Us_needed-1]
    Wnode[Us_needed*2 -1] ^ last_U_bra_node[1]

    allNodes = [last_U_node, Wnode, last_U_bra_node]
    for i in range(1, Us_needed):
        new_U_node = tn.Node(Umat, name=f"U{i}")
        new_U_bra_node = tn.Node(U_bra_mat, name=f"U_bra{i}")
        last_U_node[1] ^ new_U_node[0]
        new_U_bra_node[2] ^ last_U_bra_node[0]

        new_U_node[2] ^ Wnode[Us_needed-1-i]
        Wnode[Us_needed*2-1-i] ^ new_U_bra_node[1]

        allNodes.extend([new_U_node, new_U_bra_node])
        last_U_node, last_U_bra_node = new_U_node, new_U_bra_node

    inward_dangling_edges = [last_U_bra_node[0], last_U_node[1]]

    if plot_network:
        plot_tensor_network(allNodes)

    return tn.contractors.greedy(allNodes, output_edge_order=[*inward_dangling_edges, *outward_dangling_edges]).tensor.reshape(( (2**bond_dim)**2, (2**bond_dim)**2))


def wrap_with_transfer_matrix_circ(wrapped_circ, ketMPS: iMPS, braMPS: iMPS, W, conjugated=False, trotterization_order=1):
    """
    the circuit for the transfer matrix wrappes the left part of the mps between two U gates because
    when looking at the mps on a quantum circuit each U gate is followed by another shifted U gate
    correspoindin to the matrix to its left in the MPS.
    """
    bond_dim = ketMPS.bond_dim
    iqpU = ketMPS.initialized_qubits_per_U
    assert iqpU == braMPS.initialized_qubits_per_U, "U matrices for both mps must have same dim to get the transfer matrix"
    assert bond_dim <= wrapped_circ.num_qubits, f"the wrapped circuit must act on at least bond_dim = {bond_dim} qubits. But it acts on only {wrapped_circ.num_qubits}"

    if iqpU != 1:
        NotImplementedError("initialized_qubits_per_U may only be 1.")

    ketU = ketMPS.U if not conjugated else ketMPS.U_conj
    braU = braMPS.U if not conjugated else braMPS.U_conj

    qc = QuantumCircuit(2 + wrapped_circ.num_qubits)
    qc.append(ketU, range(ketU.num_qubits))
    qc.append(ketU, range(1, ketU.num_qubits+1))

    qc.compose(wrapped_circ, range(2, wrapped_circ.num_qubits+2), inplace=True)
    qc.append(W, [0,1])

    qc.append(braU.inverse(), range(1, braU.num_qubits+1))
    qc.append(braU.inverse(), range(braU.num_qubits))
    return qc

def wrap_with_transfer_matrix_n_times(wrapped_circ, ketMPS: iMPS, braMPS: iMPS, W, repetitions, conjugated=False, trotterization_order=1):
    for i in range(repetitions):
        wrapped_circ = wrap_with_transfer_matrix_circ(wrapped_circ, ketMPS, braMPS, W, conjugated, trotterization_order)
    return wrapped_circ

