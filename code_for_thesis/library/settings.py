# this settings dict can be changed in the notebook. just don't run settings = ... because this would shadow the real settings defined here.
# use settings[key] = value

from qiskit.primitives import Estimator, Sampler

simulate_defaults = {"simulated" : True,
                     "min shots": 10000,
                     "optimization algorithms": {"is noisy" :"Nelder-Mead", "not noisy": "BFGS"},
                     "delete estimator after n estimations": 100,
                     "estimator_options": {'shots':None, "approximation":True},
                     "log all jobs": False
                    }


execute_on_qc_defaults = {"simulated" : False,
                          "min shots": 10000,
                          "optimization algorithms": {"is noisy" :"Nelder-Mead", "not noisy": "Nelder-Mead"},
                          "delete estimator after n estimations": 1000,
                          "estimator_options": {'shots': 2**13, "backend": None},
                          "log all jobs": True
                         }

settings = simulate_defaults

_shared_variables = {"estimator" : Estimator(options=settings["estimator_options"])}

def set_estimator_options(options):
    settings["estimator_options"] = options
    _shared_variables["estimator"] = Estimator(options=options)

def switch_settings_to_qc():
    settings = execute_on_qc
    set_estimator_options(settings["estimator_options"])


def switch_settings_to_simulation():
    settings = simulate_defaults
    set_estimator_options(settings["estimator_options"])
