"""
functions, variables, gates etc. that are often needed for MPS simulations.
"""

import numpy as np
import matplotlib.pyplot as plt
from matplotlib.collections import LineCollection
import matplotlib.cm as cm
import matplotlib as mpl
from matplotlib.ticker import (MultipleLocator, AutoMinorLocator)

from IPython.display import display

from .settings import settings, set_estimator_options, switch_settings_to_simulation, switch_settings_to_qc
from .evironment_V import *
from .optimization_tools import *
from .measurement_tools import *
from .tools import *
from .iMPS import iMPS, get_transfer_matrix, create_U_gate, wrap_with_transfer_matrix_circ, wrap_with_transfer_matrix_n_times

# code from the time evolution paper:
from .exact_loschmidt import loschmidts

# Importing standard Qiskit libraries
from qiskit.tools.jupyter import *
from qiskit.visualization import *
#from ibm_quantum_widgets import *
from qiskit_aer import AerSimulator


from qiskit import QuantumCircuit, transpile, Aer
from qiskit.providers.ibmq import least_busy
from qiskit.tools.monitor import job_monitor
from qiskit.visualization import plot_histogram, plot_bloch_multivector, qcstyle
from qiskit.quantum_info.operators import Operator
from qiskit.circuit import QuantumCircuit, Parameter





def generate_MPS_U_gate(theta_v, theta_w):
    # create U
    U_qc = QuantumCircuit(2, name='U')
    U_qc.x(0)
    U_qc.u(theta_w,0,0,1)
    U_qc.cnot(0,1)
    U_qc.u(-theta_w,0,0,1)
    U_qc.x(0)

    U_qc.u(theta_v,0,0,1)
    U_qc.cnot(0,1)
    U_qc.u(-theta_v,0,0,1)
    U_qc.x(0)
    return U_qc.to_instruction()

def generate_custome_gates(g):
    # calc params
    sqrt_g = np.sqrt(np.abs(g))
    sqrt_1g = np.sqrt(1+np.abs(g))

    theta_v = np.arcsin( sqrt_g / sqrt_1g )
    theta_w = np.arccos(np.sign(g) * sqrt_g / sqrt_1g)
    theta_r = 2 * np.arcsin(1 / sqrt_1g)

    # create U_1
    U1_qc = QuantumCircuit(2, name='U_1')
    U1_qc.h(0)
    U1_qc.cnot(0,1)
    U1_qc.u(theta_r, 0, np.pi, 1)
    if g>0:
        U1_qc.h(1)
        U1_qc.cnot(0,1)
        U1_qc.h(1)
    U_1 = U1_qc.to_instruction()

    U = generate_MPS_U_gate(theta_v, theta_w)

    return U_1, U


def get_eigenvectors_per_latice(observable_string):
    """
    Parameters:
        observable_string: string that consists of 'x', 'y', 'z' and '1'.

    Returns:
        list that contains the eigenvector with eigenvalue 1 for the corresponding pauli matrix.
        or None for 1 implying identity.
    """
    char_to_vector_map = {'1': None,
                          'x': 1/np.sqrt(2) * np.array([1,1]),
                          'y': 1/np.sqrt(2) * np.array([1,1j]),
                          'z': np.array([1,0])
                         }
    return [char_to_vector_map[char] for char in observable_string]


def create_Observable_alignment_gate(Observable_eigenvectors_per_latice):
    O_qc = QuantumCircuit(len(Observable_eigenvectors_per_latice), name='U_O')
    for i, vec in enumerate(Observable_eigenvectors_per_latice):
        if type(vec) == None:
            # this latice has operator prop to Identity.
            pass
        unitary = Operator([
            np.conjugate(np.array([-vec[1], vec[0]])),
            vec
        ])
        O_qc.unitary(unitary, [i], label='U')
    return O_qc.to_instruction()


def create_final_circ(U_1, U, U_O, draw=False):
    # create circuit
    N=U_O.num_qubits+2
    circ = QuantumCircuit(N,N-2)
    circ.append(U_1, [0,1])
    for i in range(1,N-1):
        circ.append(U, [i,i+1])
    circ.barrier()
    circ.append(U_O, range(1,N-1))

    if draw:
        display(circ.draw(style={'displaycolor': {'U'    : ('#D4EBD4', '#000000'),
                                          'U_1': ('#D1E3F0', '#000000'),
                                          'U_O'  : ('#A1A1A1', '#000000')},
                         'displaytext': {'U_O': 'U_{\\hat O}^{(j)}', 'U_1': 'U_1'},
                        }))
    return circ


def calc_expectation_value(counts):
    """
    (for first simulation method, not interferometry)

    ! currently still assumes that there was no Identity in the Tensorproduct !
    counts can also be the probabilities. it will be normed any way.
    just make sure to only include those qubits that are also
    measured and written to the classical registers.
    """
    counts /= counts.sum()
    N=np.log2(len(counts))+2
    assert np.round(N) == N
    expectation_value = 0
    for i, count in enumerate(counts):
        expectation_value += counts[i] * (-1)**(1+bin(i).count('1'))
    return expectation_value


def create_final_interferometry_circ(U_1, U, observable_string, draw=False):
    # create circuit
    N = len(observable_string) + 3
    circ = QuantumCircuit(N,1)
    circ.append(U_1, [1,2])
    for i in range(2,N-1):
        circ.append(U, [i,i+1])
    circ.barrier()
    circ.h(0)
    for i, char in enumerate(observable_string):
        if char=='x':
            circ.cx(0,i+2)
        elif char=='y':
            circ.cy(0,i+2)
        elif char=='z':
            circ.cz(0,i+2)
        else:
            raise ValueError("observable string invalid.")
    circ.h(0)

    if draw:
        display(circ.draw(style={'displaycolor': {'U'    : ('#D4EBD4', '#000000'),
                                          'U_1': ('#D1E3F0', '#000000'),
                                          'U_O'  : ('#A1A1A1', '#000000')},
                         'displaytext': {'S_O': 'S_O', 'U_1': 'U_1'},
                        }))
    return circ



def plot_state_vector(state, fig=None, ax=None, include_labels_colorbar_etc=True):
    if fig==None:
        fig=plt.figure()
        ax=plt.gca()

    absolut, angle = np.abs(state), np.angle(state)
    x = np.arange(len(absolut))
    norm = plt.Normalize(-np.pi, np.pi)

    if len(absolut) < 1000: # barplots sind langsamer. Aber das ist dann okay.
        plot = ax.bar(np.arange(len(absolut)), absolut, color=cm.gist_rainbow(norm(angle)))
    else:
        ax.scatter(x, absolut, 2, color=cm.gist_rainbow(norm(angle)))

    if include_labels_colorbar_etc:
        sm = cm.ScalarMappable(cmap=cm.gist_rainbow, norm=norm)
        sm.set_array([])
        cbar = fig.colorbar(sm, cmap=cm.gist_rainbow, ax=ax)
        ax.set_xlabel("basis state")
        ax.set_ylabel("absolute value of amplitude, color gives phase")
