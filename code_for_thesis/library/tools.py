import numpy as np

import jsonpickle

from qiskit import QuantumCircuit, execute, transpile
from qiskit import Aer
from qiskit.providers.ibmq.runtime import RuntimeJob
from qiskit.primitives.base.estimator_result import EstimatorResult
from qiskit import transpile
from qiskit.circuit import Gate

from .settings import settings


unitary_simulator = Aer.get_backend('unitary_simulator')

def filter_params(params, circ):
    """
    return only the parameter that circ actually needs.
    raises key Error if a needed parameter is not present in params.
    """
    return {key: params[key] for key in circ.parameters}

def get_params(circ_or_similar):
    try:
        # works if circ_or_similar is instruction.
        return circ_or_similar.params
    except AttributeError:
        # works for TwoLocal or circuit
        return circ_or_similar.parameters

def join_params(*list_of_params):
    """
    join parameter dicts (or any dict) with overwriting protection.
    raises: AssertionError if there would be some overwriting. It does not raise if the candidate definitions are the same.
    returns joined dictionary.
    """
    all_params = list_of_params[0]
    for params in list_of_params:
        shared_param_n = set(all_params).intersection(set(params))
        for param_n in shared_param_n:
            assert all_params[param_n] == params[param_n], f"at least two parameter dictionaries define the parameter(s) {shared_param_n} and at least for {param_n} the definitions are not the same."
        all_params = {**all_params, **params}
    return all_params

def get_matrix_for_gate(gate, params={}, precission=4):
    circuit = QuantumCircuit(gate.num_qubits,gate.num_clbits)

    circuit.append(gate, range(gate.num_qubits))

    several_params=False
    if type(params) in (list, tuple) and type(params[0]) == dict:
        several_params=True
        circuits = [circuit.bind_parameters(params_) for params_ in params]
    else:
        circuits = circuit.bind_parameters(params)
    job = execute(circuits, unitary_simulator, shots=None)
    result = job.result()

    if several_params:
        return [result.get_unitary(i, precission) for i in range(len(params))]
    return result.get_unitary(None, precission)



def _complex_conjugate_circuit(circuit):
    """
    takes a circuit and returns its complex conjugate.
    i.e. the matrix representing the combined effect of
    the gates will be complex conjugated. (Not adjoint)

    there might be a global phase added etc.
    """

    # Transpile the circuit to the RX, RZ, and CZ gate set
    transpiled_circuit = transpile(circuit, basis_gates=['rx', 'rz', 'cz'], seed_transpiler=3)

    # Create a new circuit with the same registers as the transpiled circuit
    new_circuit = QuantumCircuit(*transpiled_circuit.qregs, name=circuit.name + "^*")

    # Iterate over the gates in the transpiled circuit
    for inst, qargs, cargs in transpiled_circuit.data:
        # If this is an RZ or RX gate, invert its parameter
        if isinstance(inst, Gate) and (inst.name == 'rz' or inst.name == 'rx'):
            inst.params[0] = -inst.params[0]
        # Add the gate to the new circuit
        new_circuit.append(inst, qargs, cargs)

    return new_circuit

def complex_conjugate_instruction(instruction):
    # Create a new quantum circuit with the same number of qubits as the instruction
    circuit = QuantumCircuit(instruction.num_qubits, name=instruction.name)
    # Add the instruction to the circuit
    circuit.append(instruction, range(instruction.num_qubits))
    circuit_conjugated = _complex_conjugate_circuit(circuit)
    return circuit_conjugated.to_instruction()



def relu(x):
    return x*(x>0)


def compare_functions(f1, f2, domain=(0,2*np.pi), allow_const_dist=True, tol=1e-10, context=""):
    dist = f1(domain[0]) - f2(domain[0])
    dist_not_constant = False
    for theta in np.linspace(*domain, 100):
        if abs((f1(theta) - f2(theta)) - dist) > tol:
            dist_not_constant = True
            print(f"theta: {theta}, f1(theta): {f1(theta)}, f2(theta): {f2(theta)}, f1(theta)-f2(theta)-dist: {f1(theta)-f2(theta)-dist} ({context})")
    if dist_not_constant:
        return False
    if not allow_const_dist and dist > tol:
        print(f"--comparison: functions are different by constant dist: {dist}. (that is not allowed)")
        return False
    return True


def init_IBM_connection():
    # qiskit-ibmq-provider has been deprecated.
    # Please see the Migration Guides in https://ibm.biz/provider_migration_guide for more detail.
    from qiskit_ibm_runtime import QiskitRuntimeService, Session
    from qiskit_ibm_provider import IBMProvider

    # Loading your IBM Quantum account(s)
    service = QiskitRuntimeService(channel="ibm_quantum")
    try:
        provider = IBMProvider()#hub='ibm-q')
    except:
        print("you need to save the API Token of your IBMQ-Account first")
        print("use `IBMProvider.save_account(token=<INSERT_IBM_QUANTUM_TOKEN>)` once")
        raise
    return service, provider

def log_job_if_wanted(*args, **kwargs):
    if settings["log all jobs"]:
        save_job(*args, **kwargs)
    elif not settings["simulated"]:
        save_job_id_context(*args, **kwargs)

def save_job_id_context(job, context, options):
    with open(f"results/jobs/__job_ids.txt", "a") as f:
        f.write(f"{job.job_id()};{context};{dict(options)}\n")

def save_job(job, context, options):
    save_job_id_context(job, context, options)
    res = job.result()
    if type(res) != dict:
        try:
            res = res.to_dict()
        except:
            if isinstance(res, EstimatorResult):
                res = {"values": [float(value) for value in res.values], "metadata": res.metadata}
    res = {**res, "context": context, "options" : dict(options)}
    data = jsonpickle.encode(res)

    with open(f"results/jobs/{job.backend()}_{job.job_id()}.json", "a") as f:
        f.write(data)

import networkx as nx
import matplotlib.pyplot as plt
def plot_tensor_network(nodes):
    """
    Create a new NetworkX graph
    unfortunatly dangling edges are currently shown as lines going back into the same node.
    """

    G = nx.Graph()

    # Add nodes to the graph
    for node in nodes:
        G.add_node(node.name)

    # Add edges to the graph
    for node in nodes:
        for edge in node.edges:
            if edge.node1 is not None and edge.node2 is not None:
                G.add_edge(edge.node1.name, edge.node2.name)
            elif edge.node1 is not None:
                G.add_edge(edge.node1.name, edge.node1.name)
            elif edge.node2 is not None:
                G.add_edge(edge.node2.name, edge.node2.name)

    # Draw the graph
    nx.draw(G, with_labels=True)
    plt.show()
