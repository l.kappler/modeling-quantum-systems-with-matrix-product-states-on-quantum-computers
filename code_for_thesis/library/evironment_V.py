"""
This library provides all functions to apply and optimize the environment V.

This is based on "Finding the Environment" in Supplemental materials of DOI:10.1038/s41534-021-00420-3
"""

import numpy as np
from IPython.display import display

from .tools import filter_params, join_params
from .optimization_tools import double_rotosolve
from .measurement_tools import create_obs_from_string, simulate_circ, estimate
from .settings import settings

# Importing standard Qiskit libraries
from qiskit import QuantumCircuit, transpile
from qiskit.tools.jupyter import *
from qiskit.visualization import *
#from ibm_quantum_widgets import *
from qiskit_aer import AerSimulator

# qiskit-ibmq-provider has been deprecated.
# Please see the Migration Guides in https://ibm.biz/provider_migration_guide for more detail.
from qiskit_ibm_runtime import QiskitRuntimeService, Sampler, Session, Options

from qiskit import QuantumCircuit, transpile, Aer, IBMQ
from qiskit.providers.ibmq import least_busy
from qiskit.tools.monitor import job_monitor
from qiskit.visualization import plot_histogram, plot_bloch_multivector, qcstyle
from qiskit.quantum_info.operators import Operator
from qiskit.quantum_info import partial_trace




from qiskit.circuit import QuantumCircuit, Parameter

def create_V_gate(name, bond_dim, draw_circ=False):
    """
    following the paper DOI: 10.1038/s41534-021-00420-3 > Supplemental materials.
    after swapping the initialized bond qubits to the right position, the i'th bond qubit will depend on the parameters v^i.
    """
    parameter_suffix=name.lower()
    V_template = QuantumCircuit(bond_dim*2, name=name)

    for i in range(bond_dim):
        if i < bond_dim//2:
            final_qubit_index = (i + (bond_dim+1)//2)*2 - bond_dim
        else:
            final_qubit_index = i *2 -bond_dim +1
        theta_p = Parameter(f"${parameter_suffix}^{final_qubit_index}_p$")
        theta_1 = Parameter(f"${parameter_suffix}^{final_qubit_index}_1$")
        theta_2 = Parameter(f"${parameter_suffix}^{final_qubit_index}_2$")

        V_template.rx(theta_p, 2*i)
        V_template.cx(2*i, 2*i + 1)
        V_template.rx(theta_1, 2*i + 1)
        V_template.rz(theta_2, 2*i + 1)

    for i in range(bond_dim//2):
        # now we need to swap the upper correctly initialized qubits at i*2+1
        # with the lower ancilla qubits that is (bond_dim+1)//2 *2 -1 further down.
        # // is whole number division.
        V_template.swap(i*2+1, (i + (bond_dim+1)//2) * 2)

    if draw_circ:
        display(V_template.draw())
    return V_template.to_instruction()


# define the circuits used to find the environment V:
def get_circuits_for_V_optimization(U, V_parameterized):
    """
    U: the U gate which embeds the MPS matrix.
    V: a prameterized V gate that should be inserted in the circuits.
    """

    qc_tr_σσ = QuantumCircuit(6)
    qc_tr_σσ.append(V_parameterized, [0,1])
    qc_tr_σσ.append(U, [1,2])

    qc_tr_σσ.append(V_parameterized, [3,4])
    qc_tr_σσ.append(U, [4,5])
    qc_tr_σσ.cx(5,2)
    qc_tr_σσ.h(5)

    # !!! the order of qubits is opposite to what we see in textbooks.
    # see https://qiskit.org/documentation/explanation/endianness.html
    obs_tr_σσ = create_obs_from_string("1II1II")
    3

    qc_tr_ρσ = QuantumCircuit(5)
    qc_tr_ρσ.append(V_parameterized, [0,1])
    qc_tr_ρσ.append(U, [1,2])

    qc_tr_ρσ.append(V_parameterized, [3,4])
    qc_tr_ρσ.cx(4,2)
    qc_tr_ρσ.h(4)

    obs_tr_ρσ = create_obs_from_string("1I1II")


    qc_tr_ρρ = QuantumCircuit(4)
    qc_tr_ρρ.append(V_parameterized, [0,1])

    qc_tr_ρρ.append(V_parameterized, [2,3])
    qc_tr_ρρ.cx(3,1)
    qc_tr_ρρ.h(3)

    obs_tr_ρρ = create_obs_from_string("1I1I")

    return (("σσ", qc_tr_σσ, obs_tr_σσ, 1), ("ρσ", qc_tr_ρσ, obs_tr_ρσ, -2), ("ρρ", qc_tr_ρρ, obs_tr_ρρ, 1))




def estimate_trace_of_difference(U, V_parameterized, param_list):
    """
    estimate the trace of the difference of the local density matrices tr((ρ-σ)^\dagger (ρ-σ))
    This needs to be minimized to 0 for the two density matrices to be equal.

    Note: The authors of the above referenced paper say they would count the 00 outputs.
    However the source they reference there counts 11 outputs and they them self say
    they use 11 outputs in the main notebook of their code basis.

    I use 11 outputs as well and the formula in the paper DOI:10.1103/PhysRevA.87.052330 on the swap test
    This paper only handles pure states. I show in my Bachelorthesis that this also works for density matrices.
    """
    was_not_list = False
    if type(param_list) not in [list, tuple, np.ndarray]:
        was_not_list = True
        param_list = (param_list,)
    trace_circuits = get_circuits_for_V_optimization(U, V_parameterized)

    #result = 0
    #axis=iter([(0,1,3,4), (0,1,3), (0,2)])
    #for name, circ, obs, factor, in trace_circuits:

        #traspiled_circ = transpile(circ.bind_parameters(params), backend=aer_sim)
        #job = aer_sim.run(traspiled_circ)
        #statevector = job.result().data()["statevector"]
        #value = np.abs(np.array(statevector).reshape(statevector.dims())**2).sum(axis=next(axis))[1,1]


    names, circuits, obs, factors = zip(*trace_circuits)
    circuits_with_params_binded = []
    for params in param_list:
        for circ in circuits:
            circuits_with_params_binded.append(circ.bind_parameters(filter_params(params, circ)))
    res = estimate(circuits_with_params_binded, obs*len(param_list), context=f"circuit: estimate trace of difference; param list: {param_list}")
    values = np.array(res.values).reshape((-1,3))
    traces = 1-2*values
    results = traces @ np.array(factors)
    if was_not_list:
        return results[0]
    return results


def estimate_trace_of_difference_classically(U, V_parameterized, params, return_density_matrices=False):
    """
    create seperate circuit to simulate and compare the classically calculated
    density matrices before the swap test.
    If return_density_matrices, they return value will be: tr(...), σ, ρ
    """

    qc_ρσ = QuantumCircuit(5)
    qc_ρσ.append(V_parameterized, [0,1])
    qc_ρσ.append(U, [1,2])
    qc_ρσ.append(V_parameterized, [3,4])

    statevector, _ = simulate_circ(qc_ρσ.bind_parameters(params))

    σ = np.array(partial_trace(statevector, [0,1,3,4]))
    ρ = np.array(partial_trace(statevector, [0,1,2,3]))
    #print(σ,ρ,σ-ρ, sep="\n")
    if return_density_matrices:
        return np.trace((ρ-σ).T.conj()@(ρ-σ)).real, σ, ρ
    return np.trace((ρ-σ).T.conj()@(ρ-σ)).real

def find_environment_V(U, V_parameterized, V_init_params, other_parameters = {}, **optimization_kwargs):
    """
    optimizes the parameters of the V environment.
    U: the gate embedding the MPS matrix
    V_parameterized: the gate of the environment V that is parameterized by:
    V_init_params: inital parameters for the optimization
    other_parameters: parameters for U if existent.

    optimization_kwargs:
    tollerance: tollerance for accepting a minimum. It's how much the parameters changed in one iteration
                and in this case how far away from 0 the function may be at the end.
    max_iterations: how many iterations the double_rotosolve algorithm may take at most.
    quiet: whether to print current params after iterations etc.
    """
    cost_func = lambda param_list : estimate_trace_of_difference(U, V_parameterized, [join_params(other_parameters, params) for params in param_list])
    optimization_kwargs = {"desired_minimum": 0, **optimization_kwargs}
    return double_rotosolve(cost_func, V_init_params, **optimization_kwargs)
