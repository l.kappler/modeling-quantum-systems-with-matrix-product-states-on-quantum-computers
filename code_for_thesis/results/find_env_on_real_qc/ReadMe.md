This directory contains the results of the optimizations of the environment V using simulations with varying shot numbers as well as a quantum computer.

The .csv files contain in the first columns the values of the trace from the simulations. The files ending in trace.pdf plot these columns for different configurations
The other columns contain the parameter of V used to get those traces.

(see sec. 4.2 in the thesis.)
