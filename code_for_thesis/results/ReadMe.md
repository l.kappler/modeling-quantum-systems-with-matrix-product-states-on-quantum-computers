# Results
This directory contains all results shown in the Thesis. This includes all the plots, all the data plotted as .csv and all the raw data from the optimizations i.e. the optimized Parameter of the circuits.

## directories

This dir contains the last results from the transversal field ising (tfi) model, to my knowledge bug free... The child dirs:

- `check_convergence_for_finding_the_environment`: contains the results from 20 runs of the optimization to find L for different random U to see that the circuits from https://www.nature.com/articles/s41534-021-00420-3 don't work, while mine do. (This is probably just because it misses some conjugation, transpose and inversion signs. See page 42f in the thesis.)
- `find_env_on_real_qc`: optimizing the environment V for different U params. includes three interations on a real quantum computer. Results are disscussed in 4.2
- `jobs`: all the results from the jobs that where executed on a quantum computer and the qasm_simulator at IBM. `__job_ids.txt` contains context for the jobs as defined in library.tools.save_job_id_context (not very human readable)
- `other_figures`: all the diagrams for explaining the algorithms in the thesis. Code is in `../create plots for explanation in thesis.ipynb`


## naming schema for this dir and its children
This third Hamiltonian stands for the transversal field ising (tfi) model. Previous models did not work well, because the time evolution created to much entanglement which was outside the manifold of the MPS with bond dim 2.

- `with_quench`: I didn't see from the beginning that it is only possible to get the spikes if the system stated from the groundstate with different parameters g.
             The g parameters are the coefficients in front of the X term. All files in this dir use the quench.

- `eigenvalue^i_overlap` means that I took the transfer matrix to the power of i and then calculated the largest eigenvalue by absolute value.

  - `classically`: means that this eigenvalue was calculated with scipy.linalg.eig not with some circuit. (should have been more accurate)
  - `optimizing_l_tn`: $L$ was found by optimizing $\frac{|LE-L|^2}{|L|^2}$ or the approximation from eq. 3.10 by contracting the corresponding tensor networks for LE and computing the rest classically. Then the eigenvalue was found using eq. 3.11 and the corresponding tensornetworks.
  - `L_min_approx:True/False` : (False if not given) wether the approximation in (3.10) was used.


- `g=...` what g parameters where used. (The parameter given in the time evolution paper didn't match the ones I found in their code.)
        First value is the one used for the ground state second after the quench.
        (In this folder its always g=1 for the groundstate and g=.2 afterwards, typically with the convention {"factor_ZZ": 1, "factor_X": g_i}) (see with_zz)


- `icnot_in_U` : If this string is in the file name I used i reps without the final rotation layer which means i entanglement and i rotation layers.
                (all the simulations with quench have this.) If this string is not in the filename I used a TwoLocal gate with 2 reps and with the
                final rotation layer. i.e. two entanglement layers and three rotation layer.


there were differnt conventions in the hamiltonian. the time evolution paper used the same sign for zz and x term and no 1/2 factor, while the loschmidt paper used -1/2 for zz and g/2 for x.
- `with_zz:-.5` means loschmidt paper convention, i.e. {"factor_ZZ": -1/2, "factor_X": g_i/2})
- `with_zz:1` means other convention, i.e. {"factor_ZZ": 1, "factor_X": g_i}) (this is used in this folder)

- `dt` time difference between steps. if it is not given it is .01

- `failed`: eigenvalues of transfermatrix where not anywhere near 1 i.e. the optimization failed completely.
- `Nelder-Mead`/`BFGS` : optimiation algorithm used for maximizing the local scalar product.
- `non_iterative` : instead of iteratively evolving the state, each time step is generated from the initial state with an increased delta t. This does not work well, because the condition described in A.2.1 that allowed us to use the third approximation is not given anymore.

- `__U_params.csv` contains the U parameter for each time step.
- `__Loschmidt_Echos.csv` contains $-\log|\langle \psi(t) | \psi(0)\rangle |^2$ for the corresponding U params file, i.e. the data that is plotted in the pdfs. (only saved for those used in the thesis.)
- pdfs that contain more than one curve are typically named after the last one in the legend and the title.


### groundstate

`ground_state_params_{reps}cnot_in_U__g=1_with_zz:1...` are the files containing the parameter from the ground states. only the last one `..._3` has the correct Energy eigenvalue of approximately -1.27(1) J. The other did not converge well. This good ground state was used for all the time evolutions. `reps` gives the number of repetitions of figure 3.10 in the middle.
