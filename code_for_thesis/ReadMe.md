# Environment Setup

## Creating the Conda environment

Create a new Conda environment with the required dependencies:
```bash
conda create --name qiskit-env --file dependencies.txt
```


Activate the new environment:

```bash
conda activate qiskit-env
```


## Installing additional packages using pip

Install additional packages using `pip`:

```bash
pip install -r requirements.txt
```

## automatic update of dependencies

in git_hooks the pre-commit file contains code to update the dependency files before committing. It assumes the environment name given here.
In the repos root directory run `ln -s ../../git_hooks/pre-commit .git/hooks/pre-commit` to use this hook.


## Qiskit setup

to draw circuits with matplotlib and the style given in this directory by default write
```conf
[default]
circuit_drawer=mpl
circuit_mpl_style = circuit_drawing_style
```
into `~/.qiskit/settings.conf` or where ever your config file is.


# Directories

- `library` contains functions and classes used in the notebooks
- `implementation tests` contains tests of some functions in `library`
- `results` contains all the results of the thesis as well as a few failed attempts
- `some tests` contains basic tests of some quantum computer algorithms such as the Quantum Fourier Transform.

# Files

- `Time evolution first try.ipynb` still using the U and V from DOI:10.1103/physrevresearch.4.l022020 and testing optimizing L for the time evolution algorithm. Also showing, that the circuits for optimizing L from DOI:10.1038/s41534-021-00420-3 don't work and implementing alternative circuits
- `Time_evolution_LoschmidtEcho-final.ipynb` : The final code used for the time evolutions in the thesis.
- `Time_evolution_LoschmidtEcho-create-plots.ipynb` : Code to create the Loschmidt plots shown in the thesis.
- `optimize environment on qc.ipynb` code used to create the plots in section 4.2, i.e. finding the environment V
- `dependencies.txt`, `requirements.txt`: see Setup
- `circuit_drawing_style.json` : as defined in the config for quiskit in the setup this file contains style information for the different gates etc.

Note that in this git repository certain files like buggy code or results generated from buggy code have been removed. PD. Dr. Salvatore R. Manmana still has access to all the files via the git repo I used while working on this thesis.

## Not so important notebooks:
- `check swap test for density matrix.ipynb` : prove that the swap test works also for density matrix states using sympy. (The derivation in the thesis is probably easier to understand.)
- `create plots for explanation in thesis.ipynb` : contains the code that created all the plots of the explanatory part of the thesis, i.e. those stored in `results/other_figures`.
- `MPS.ipynb` : code for computing expectation values on a quantum computer. Based on DOI:10.1103/physrevresearch.4.l022020

# Things I changed in the algorithm
- when optimizing the environment V, I counted 11 outcomes instead of 00 as the time evolution paper suggested for comparing density matrices. See p. 31 in my thesis.
- The circuits for finding $L$ are different (see end of ch. 3.3.2)
- I'm just using the left eigenvalue of the transfer, the matrix R is not needed anymore. (see ch. 3.3.2)
- I'm using cnot with target and control switched in implementation of R and L (see A.4 in thesis)
- I interpreted the X^φ in the time evolution paper as rx(φ). It turns out that cirq uses the gates rx and xpowgate which only differ by a global phase. Similarly for y and z.
- I'm not using 2*θ in the parameterization of L to retain the relation to the Schmidt coefficients in the circuit of the mixed environment.
- probably other optimization algorithms had been used. I don't know which the time evolution paper used.
