%% LyX 2.3.6 created this file.  For more info, see http://www.lyx.org/.
%% Do not edit unless you really know what you are doing.
\documentclass[english]{article}
\renewcommand{\familydefault}{\sfdefault}
\usepackage[utf8]{inputenc}
\usepackage[a4paper]{geometry}
\geometry{verbose}
\setlength{\parskip}{\medskipamount}
\setlength{\parindent}{0pt}
\usepackage{color}
\usepackage{babel}
\usepackage{float}
\usepackage{mathtools}
\usepackage{amsmath}
\usepackage{graphicx}
\usepackage[unicode=true,
 bookmarks=false,
 breaklinks=false,pdfborder={0 0 1},backref=false,colorlinks=true]
 {hyperref}
\hypersetup{
 allcolors=blue}

\makeatletter

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% LyX specific LaTeX commands.
\floatstyle{ruled}
\newfloat{algorithm}{tbp}{loa}
\providecommand{\algorithmname}{Algorithm}
\floatname{algorithm}{\protect\algorithmname}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% User specified LaTeX commands.
\include{preamble_for_lyx_files}

\makeatother

\usepackage{listings}
\usepackage[bibstyle=authoryear,style=alphabetic]{biblatex}
\renewcommand{\lstlistingname}{\inputencoding{latin9}Listing}

\addbibresource{literatur.bib}
\begin{document}
\input{macros_for_lyx.tex}

Modeling quantum many-body systems is important for drug design \autocite{caoPotentialQuantumComputing2018},
condensed matter physics and many other research areas that rely on
quantum physics. With better simulation capabilities, we would be
able to design more effective drugs with fewer side effects, better
computer chips, batteries, fuel cells, etc. However, the resource
requirements for exact simulations on a classical computer grow exponentially
with system size, as will be explained in this thesis.  This limits
the size of systems we can accurately simulate. A quantum computer
is a different type of computer that exploits quantum mechanical effects
such as superposition and entanglement. Such devices could hopefully
simulate quantum systems while scaling linearly or at least polynomially
with system size. Although Peter Shor's algorithm \cite{shor-factor}
for prime factorization, which cracked the RSA (Rivest-Shamir-Adleman)
cryptosystem, initially captivated the public, the most practical
applications will likely continue to center around the simulation
of quantum systems, as envisioned by Yuri I. Manin \cite[Appendix of][]{maninClassicalComputingQuantum1999}
and Richard Feynman \cite{feynmanSimulatingPhysicsComputers1982}.
For quantum computers to outperform classical supercomputers, we would
need many more qubits, which are the quantum equivalent of bits in
a classical computer's memory. The current record is 433 qubits from
IBM. In addition, existing qubits are noisy e.g. their states collapse
rapidly, destroying superposition and entanglement. For this reason
current quantum computers are often called Noisy Intermediate Scale
Quantum Computers (NISQ). To obtain accurate results, we would have
to use quantum error correction codes, which require even more qubits.
Nevertheless, with the quantum computers currently available at \cite{IBM-QuantumComputing-presentation},
it is already possible to perform small tests.

Specifically I try to simulate an infinite translationally invariant
spin-$\frac{1}{2}$ chain on a quantum computer. Using certain tricks,
simulating such systems, where each lattice site is in the same state,
is not much more complicated than simulating a small spin-$\frac{1}{2}$
chain. We can gradually approximate the ground state of a Hamiltonian
or a time-evolved state by minimizing cost functions evaluated on
a quantum computer using classical optimization procedures. The cost
functions we can evaluate on a quantum computer have some limitations,
but these are not very problematic in this use case. The advantage
is that these cost functions scale more efficiently on quantum computers
than with classical simulations.

The methodology used is based on \cite{Smith_2022,barrattParallelQuantumSimulation2021}
and borrows ideas from Matrix Product States (MPS), a method to approximate
quantum states on classical computers. The MPS algorithm is one of
many that can be represented as a tensor network. In general translating
tensor networks to quantum circuits as suggested in the Discussion
of \cite{barrattParallelQuantumSimulation2021} has the potential
to speed up well developed classical methods.

The thesis is structured as follows: First \autoref{chap:classical_basics}
introduces the classical methods used, including MPS. This is followed
by an introduction to quantum computing in \autoref{chap:impl_on_qc}
where I also describe general steps to translate a tensor network
to a quantum algorithm. In \autoref{chap:results} results are presented
from simulations and real quantum computers. At last, \autoref{chap:conclusion}
contains the conclusion where I discuss the problems and benefits
with the developed algorithm and an outlook.

\section{What is a Quantum Computer?\label{sec:What-is-a-qc}}

Classical computers have a memory chip for storing information in
classical bits and a central processing unit (CPU) that manipulates
the information. In contrast quantum computers have a memory chip
with qubits that are manipulated there. The difference of qubits to
classical bits is that the qubits are in quantum mechanical states.
The Hilbert space of the states of one qubit is spanned by the computational
basis $|0\rangle$ and $|1\rangle$. The space of possible states
of all qubits together is the tensor product of the Hilbert spaces
of each qubit. This allows them to be entangled and put into superpositions
of any combination of $|0\rangle$ and $|1\rangle$ states. The entire
memory chip is in a quantum state and can represent the quantum state
of a physical system. At the end of an algorithm, measurements on
the qubits of interest are performed and yield a 0 or a 1 per qubit
with the probabilities governed by the rules of quantum mechanics.

There are several proposed ways to create qubits, many of which are
listed in \cite{laddQuantumComputers_realizationMethods2010}. For
example, one could isolate the spin of a trapped ion, nuclide or quantum
dot and use the spin direction to represent $|0\rangle$ and $|1\rangle$.
Simply put, for a single qubit, the system is manipulated by applying
transverse magnetic fields that cause the spin to precess around the
axis of the field. Such operations are called single-qubit gates.
For interactions of qubits, electric fields are applied to move the
two spins closer together \cite{kloeffelProspectsSpinBasedQuantum2013}.
Coils can be used to measure the magnetic field of the spin \cite{laddQuantumComputers_realizationMethods2010}.
The most promising method is the use of Josephson junctions \cite{makhlin-Josephsonjunction_QubitsRealization1999}.
These are comparatively macroscopic superconducting LC-resonators
i.e. circuits with a coil and a capacitor. Nevertheless, they are
narrow enough to exhibit quantum mechanical properties and, for sufficiently
low energy ranges, i.e. temperatures, can be described by two basis
states corresponding to $|0\rangle$ and $|1\rangle$. Due to their
relatively small size, they are relatively easy to build, but suffer
from short decoherence times \cite{laddQuantumComputers_realizationMethods2010}.
This means that we have less time to run the circuit because the wave
function collapses earlier, rendering the results unusable.

\subsection{Example: Quantum Fourier Transformation \label{subsec:Example:-QFT}}

The following is based on \cite{quiskit-qft}. Many algorithms, such
as the Shor algorithm \cite{shor-factor} for factoring numbers, use
the discrete and complex Quantum Fourier Transform (QFT). The input
of the transformation is a superposition of numbers represented in
binary on the qubits. For example $\frac{1}{\sqrt{2}}\left(|7\rangle+|5\rangle\right)\equiv\frac{1}{\sqrt{2}}\left(|0...0111\rangle+|0...0101\rangle\right)$
etc. This is called the computational basis and will be used throughout
this Thesis. The QFT acts on the complex amplitudes (coefficients)
of each basis state and replaces them with the amplitudes in frequency
space:

\begin{gather*}
c_{0}|0\rangle+c_{1}|1\rangle+...+c_{2^{n}-1}|2^{n}-1\rangle\quad\overset{\mathcal{\text{QFT}}}{\longrightarrow}\quad\tilde{c}_{0}|0\rangle+\tilde{c}_{1}|1\rangle+...+\tilde{c}_{2^{n}-1}|2^{n}-1\rangle\\
\mathllap{\text{with}\qquad}\tilde{c}_{k}:=\frac{1}{\sqrt{2^{n}}}\sum_{j=0}^{2^{n}-1}c_{j}\exp\left(2\pi i\cdot\frac{j}{2^{n}}k\right)\:,
\end{gather*}

where $n$ is the number of qubits used to store the state. The computational
basis stays untouched. The algorithm works in-place on the input,
hence it does not need any carry bits or any other kind of temporary
storage. It is also extremely efficient compared to the classical
Fourier Transformation: While a classical computer has to calculate
complex exponentials involving many sums and products, QFT simply
rotates the qubits in 3D space, which naturally gives the sine and
cosine.

However, the quantum nature of the computing device prevents us from
measuring all the frequency amplitudes. Instead, we can only measure
each qubit once per run and get back a single binary number because
the system collapsed into the corresponding state $|k\rangle$. Each
$k$ has probability $|\tilde{c}_{k}|^{2}$. In other words: when
we measure in the computational basis, we typically get back one of
the prominent frequencies. By repeating the transformation and measurement
many times, we could estimate the absolute value of the amplitudes
of each frequency.

\begin{algorithm}
\inputencoding{latin9}\begin{lstlisting}[basicstyle={\small},tabsize=4,escapeinside={(*}{*)},mathescape=true,morekeywords={For, To, apply}]
init/generate amplitudes. F(*\hspace{0pt}*)or example with circ.initialize in
qiskit

For $i$ = number_of_qubits To 1:
	apply Hadamard Gate on $i$'th qubit
    For $j$ = 1 To $i-1$:
        apply controled phase shift on the $i$'th qubit controled
		      by the $j$'th with phase difference $2\pi/2^{(i-j+1)}$
For $i$ = 1 To number_of_qubits divided by 2 rounded down:
	apply swap gate on $i$'th and (number_of_qubits-$i$+1)'th qubit
\end{lstlisting}
\inputencoding{utf8}
\caption{\label{alg:Quantum-Fourier-Transformation}Quantum Fourier Transformation
in pseudo code. Derived at \cite{quiskit-qft}. In Qiskit, the least
important bit in the binary integer representation is the 1st (0th
in Python) qubit. The transformation of each qubit depends only on
the initial state of the lower indexed qubits. Therefore, start with
the highest indexed qubit and iterate down. See \ref{sec:how-to-code-on-qc}
for an explanation of the gates.}
\end{algorithm}

The circuit for the QFT can be designed on a Quantum Computer with
the pseudo code in \autoref{alg:Quantum-Fourier-Transformation}.
See \cite{quiskit-qft} for a derivation. Before running the QFT it
is essential to initialize the qubits with the complex amplitudes
that the QFT is supposed to operate on.

\subsubsection{Results}

In \autoref{fig:qft-example-runs} we can see four examples where
the simulated result of the QFT gives the correct result. As mentioned
above, the phase information is lost when the qubits are measured.
However, with an error-free quantum computer, the frequency distribution
of $k$ should approach the absolute square of the simulated complex
amplitudes. Unfortunately, we can see that this is not the case, which
means that this quantum computer is too error-prone due to collapsing
wave functions, bad calibration of the applied operations, etc. For
this particular simulation, the quantum computer code-named \inputencoding{latin9}\lstinline!ibmq_lima!\inputencoding{utf8}
was used and ran the algorithm 2048 times.

\newpage\setlength{\footskip}{3.5cm}

\begin{figure}[H]
\vspace{-2.5cm}\makebox[1\textwidth][c]{

\includegraphics[width=1.2\textwidth]{figures/figures_from_Einarbeitung/QFT_results}

}

\caption{Simulated and quantum computer results of the Quantum Fourier Transformation
with $n=4$ qubits for four different initialized states using \autoref{alg:Quantum-Fourier-Transformation}.
The plots in the first and second columns show the complex amplitudes
for each basis state, while the third column shows how often each
basis state, which translates directly to the $k$-value, was measured.
The color corresponds to the phase of the complex amplitude.\label{fig:qft-example-runs}}
\end{figure}

\setlength{\footskip}{24pt}
\end{document}
