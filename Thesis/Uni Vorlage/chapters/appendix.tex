%% LyX 2.3.6 created this file.  For more info, see http://www.lyx.org/.
%% Do not edit unless you really know what you are doing.
\documentclass[english]{article}
\renewcommand{\familydefault}{\sfdefault}
\usepackage[utf8]{inputenc}
\usepackage[a4paper]{geometry}
\geometry{verbose}
\setlength{\parskip}{\medskipamount}
\setlength{\parindent}{0pt}
\usepackage{color}
\usepackage{babel}
\usepackage{float}
\usepackage{units}
\usepackage{amsmath}
\usepackage{graphicx}
\usepackage[unicode=true,
 bookmarks=false,
 breaklinks=false,pdfborder={0 0 1},backref=false,colorlinks=true]
 {hyperref}
\hypersetup{
 allcolors=blue}

\makeatletter

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% LyX specific LaTeX commands.
%% A simple dot to overcome graphicx limitations
\newcommand{\lyxdot}{.}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% User specified LaTeX commands.
\include{preamble_for_lyx_files}

\makeatother

\usepackage[bibstyle=authoryear,style=alphabetic]{biblatex}
\addbibresource{literatur.bib}
\begin{document}
\input{macros_for_lyx.tex}

\chapter{Implementation on a Quantum computer}

\section{Example: trace of density matrices\label{sec:Appendix:-trace-of-density-matrices}}

Lets go through the steps we needed for \ref{eq:vertical-identity}
again but this time for $K_{(il)(11)}$. This time we start with the
state $|11\rangle$. 

\begin{gather*}
|11\rangle\quad\underset{\text{q0}}{\overset{\hat{H}}{\longrightarrow}}\quad\frac{1}{\sqrt{2}}\left(|10\rangle-|11\rangle\right)\quad\underset{\text{c:q0, t:q1}}{\overset{c\hat{X}}{\longrightarrow}}\quad\frac{1}{\sqrt{2}}\left(|10\rangle-|01\rangle\right)
\end{gather*}

By comparing the coefficients we can see that:

\begin{gather*}
K_{(il)(11)}:=\langle il|\hat{K}|11\rangle=\langle il|\frac{1}{\sqrt{2}}\left(|10\rangle-|01\rangle\right)=\frac{(-1)^{l}}{\sqrt{2}}(1-\delta_{il})
\end{gather*}

The other missing peace was in \ref{eq:trace-of-density-matrices-via-prob}:

\begin{gather*}
P_{11}=\sum_{j,m}\left|K_{(il)(11)}^{*}b_{ij}a_{lm}\right|^{2}=\sum_{j,m}\left|\sum_{i,l}b_{ij}a_{lm}\frac{(-1)^{l}}{\sqrt{2}}(1-\delta_{il})\right|^{2}=\frac{1}{2}\sum_{j,m}\left|b_{1j}a_{0m}-b_{0j}a_{1m}\right|^{2}\\
=\frac{1}{2}\sum_{j,m}\left(b_{1j}a_{0m}-b_{0j}a_{1m}\right)\left(b_{1j}^{*}a_{0m}^{*}-b_{0j}^{*}a_{1m}^{*}\right)\\
=\frac{1}{2}\sum_{j,m}\left(\left|b_{1j}a_{0m}\right|^{2}-b_{0j}a_{1m}b_{1j}^{*}a_{0m}^{*}-b_{1j}a_{0m}b_{0j}^{*}a_{1m}^{*}+\left|b_{0j}a_{1m}\right|^{2}\right)\\
=\frac{1}{2}\sum_{j,m}\left(\left(1-\left|b_{0j}\right|^{2}\right)\left|a_{0m}\right|^{2}-b_{0j}a_{1m}b_{1j}^{*}a_{0m}^{*}-b_{1j}a_{0m}b_{0j}^{*}a_{1m}^{*}+\left(1-\left|b_{1j}\right|^{2}\right)\left|a_{1m}\right|^{2}\right)\\
=\frac{1}{2}\sum_{j,m}\left(\left|a_{0m}\right|^{2}+\left|a_{1m}\right|^{2}-b_{0j}b_{0j}^{*}a_{0m}a_{0m}^{*}-b_{0j}a_{1m}b_{1j}^{*}a_{0m}^{*}-b_{1j}a_{0m}b_{0j}^{*}a_{1m}^{*}-b_{1j}b_{1j}^{*}a_{1m}a_{1m}^{*}\right)\\
=\frac{1}{2}\left(1-b_{ij}b_{lj}^{*}a_{lm}a_{im}^{*}\right)
\end{gather*}

where we used $\sum_{ij}|b_{ij}|^{2}=\sum_{ij}|B_{(ij)(00)}|^{2}=1$
and equivalently $\sum_{lm}|a_{lm}|^{2}=1$.

\subsection{trace of density matrices with complex conjugation\label{subsec:trace-of-density-complex-conjugation}}

We can show that:

\begin{gather}
\Tr(\rho_{1}^{*}\rho_{3})=2P_{00}\label{eq:trace-of-denstity-matrices-1}
\end{gather}

As before, this only holds if there is no entanglement between q1
and q3, which the circuit guaranties.\\

Since the qubits are once again initialized as $|0000\rangle$, we
have the factors $B_{(ij)(00)}A_{(lm)(00)}$ in the tensor network.
Let $b_{ij}=B_{(ij)(00)}$ and $a_{lm}=A_{(lm)(00)}$ be the state
vectors for qubits 3,2 and 1,0 respectively. Next we have the scaled
vertical identity over $i$ and $l$ because we once again estimate
the probability of 00 measurements. However we do not measure the
qubits corresponding to $j$ and $m$. This means: the total probability
of 00 measurements is the sum of the probabilities for the states
$|0000\rangle,|0001\rangle,|0100\rangle$ and $|0101\rangle$. So
to get the total probability of 00 we need to sum over $j$ and $m$:

\begin{gather}
P_{00}=\sum_{j,m}|b_{ij}a_{lm}\frac{1}{\sqrt{2}}\delta_{il}|^{2}=\frac{1}{2}\sum_{j,m}|b_{ij}a_{im}|^{2}=\frac{1}{2}a_{im}a_{i'm}^{*}b_{ij}b_{i'j}^{*}\label{eq:probability-from-tensor-net-1}
\end{gather}

where the factor $\frac{1}{2}$ came from squaring the factor of the
vertical identity in \ref{eq:vertical-identity}. Now lets turn to
the density matrices. As before in \ref{subsec:Example:-trace-of-density-matrices}:
\begin{gather*}
\rho_{23}=b_{(ij)}b_{(i'j')}^{*}|ij\rangle\langle i'j'|\quad\implies\quad\rho_{3}=\Tr_{2}(\rho_{23})=b_{(ij)}b_{(i'j)}^{*}|i\rangle\langle i'|\\
\text{and equivalently: }\:\rho_{1}^{*}=a_{lm}^{*}a_{l'm}|l\rangle\langle l'|\:,
\end{gather*}
 Lets calculate $\Tr(\rho_{1}^{*}\rho_{3})$:

\begin{gather*}
\Tr(\rho_{1}^{*}\rho_{3})=\Tr(a_{l'm}a_{lm}^{*}|l\rangle\langle l'|b_{ij}b_{i'j}^{*}|i\rangle\langle i'|)=a_{l'm}a_{lm}^{*}b_{ij}b_{i'j}^{*}\cdot\delta_{l'i}\delta_{i'l}=a_{im}a_{i'm}^{*}b_{ij}b_{lj}^{*}
\end{gather*}

We see that this equals double the result of \ref{eq:probability-from-tensor-net-1},
which proofs \ref{eq:trace-of-denstity-matrices-1}.

\section{translational invariant iMPS\label{sec:translation-invariant-iMPS-appendix}}

Proof that $U$ is unitary iif we use a left-normalized MPS: 
\begin{gather*}
\Id_{4}\overset{?}{=}U^{\dagger}U=\begin{pmatrix}A_{00}^{0} & A_{01}^{0} & \times & \times\\
A_{00}^{1} & A_{01}^{1} & \times & \times\\
A_{10}^{0} & A_{11}^{0} & \times & \times\\
A_{10}^{1} & A_{11}^{1} & \times & \times
\end{pmatrix}^{\dagger}\begin{pmatrix}A_{00}^{0} & A_{01}^{0} & \times & \times\\
A_{00}^{1} & A_{01}^{1} & \times & \times\\
A_{10}^{0} & A_{11}^{0} & \times & \times\\
A_{10}^{1} & A_{11}^{1} & \times & \times
\end{pmatrix}=\begin{pmatrix}\sum_{ib}\left|A_{b0}^{i}\right|^{2} & \left(A_{b0}^{i}\right)^{*}A_{b1}^{i} & 0 & 0\\
\left(A_{b1}^{i}\right)^{*}A_{b0}^{i} & \sum_{ib}\left|A_{b1}^{i}\right|^{2} & 0 & 0\\
0 & 0 & 1 & 0\\
0 & 0 & 0 & 1
\end{pmatrix}\\
\iff\qquad\Id_{2}\overset{?}{=}\begin{pmatrix}\sum_{ib}\left|A_{b0}^{i}\right|^{2} & \left(A_{b0}^{i}\right)^{*}A_{b1}^{i}\\
\left(A_{b1}^{i}\right)^{*}A_{b0}^{i} & \sum_{ib}\left|A_{b1}^{i}\right|^{2}
\end{pmatrix}=\sum_{i}\begin{pmatrix}\left(A_{b0}^{i}\right)^{*}A_{b0}^{i} & \left(\left(A^{i}\right)^{\dagger}A^{i}\right)_{01}\\
\left(\left(A^{i}\right)^{\dagger}A^{i}\right)_{10} & \left(A_{b1}^{i}\right)^{*}A_{b1}^{i}
\end{pmatrix}=\sum_{i}\left(A^{i}\right)^{\dagger}A^{i}
\end{gather*}

Here we summed over twice occurring indices $b$ and (sometimes written
explicitly) over $i$. Remember that the columns with $\times$ where
chosen to be orthonormal to the other columns, which allows us to
write the 0s and 1s in the first line RHS. We see: If $U$ is Unitary
than the MPS is left-normalized. And the other direction: If the MPS
is right-normalized and we choose the $\times$ columns accordingly,
then $U$ is unitary.

\subsection{Troterization\label{subsec:Troterization-appendix}}

Assume we have a Hamiltonian given as a sum of two qubit operators:

\begin{gather*}
\mathcal{H}=\sum_{i}h_{i}
\end{gather*}

for example in the transversal field Ising model with $\mathcal{H}=J\sum_{i}\left(\sigma_{i}^{z}\sigma_{i+1}^{z}+\lambda\sigma_{i}^{x}\right)$
we would set $h_{i}=J\sigma_{i}^{z}\sigma_{i+1}^{z}+J\lambda\sigma_{i}^{x}$
or if symmetry between $i$ and $i+1$ is needed $h_{i}=J\sigma_{i}^{z}\sigma_{i+1}^{z}+\frac{J\lambda}{2}\left(\sigma_{i}^{x}+\sigma_{i+1}^{x}\right)$.
Assume that all $h_{i}$ with $i$ even are the same and all $h_{i}$
with $i$ odd as well. Then it is possible to evolve the state with
an error of $\mathcal{O}(\Delta t)$ and $\mathcal{O}(\Delta t^{2})$,
respectively, using the first two circuits in \ref{fig:troterized-circuits}
and the following definitions:

\begin{gather*}
\text{for \ensuremath{i} even: }\;\hat{W}_{even}=e^{-ih_{i}\Delta t},\\
\text{for \ensuremath{i} odd: }\;\hat{W}_{odd}=e^{-ih_{i}\Delta t}\:,\quad\hat{W}_{odd}^{1/2}=e^{-i\frac{h_{i}}{2}\Delta t}\quad\text{and: }\;\hat{W}_{odd}^{2}=e^{-2ih_{i}\Delta t}\:,
\end{gather*}

\begin{figure}
\begin{subfigure}[h]{0.32\linewidth}\centering

\includegraphics[viewport=50bp 80bp 300bp 296bp,clip,width=1\textwidth]{figures/results/other_figures/Troterization}

\end{subfigure} \hfill \begin{subfigure}[h]{0.45\linewidth}

\includegraphics[viewport=50bp 80bp 400bp 296bp,clip,width=1\textwidth]{figures/results/other_figures/Troterization2}

\end{subfigure} \hfill \begin{subfigure}[h]{0.2\linewidth}\hfill

\includegraphics[viewport=20bp 80bp 180bp 290bp,clip,width=0.9\textwidth]{figures/results/other_figures/Troterization3}

\end{subfigure}

\caption{Left and middle: Cut out of the circuits implementing troterization
of 1st and 2nd order, respectively. Right: Circuit used in the thesis.
\cite{barrattParallelQuantumSimulation2021} claims that the error
is \textquotedblleft reduced somewhat\textquotedblright{} because of
the translational invariance with unit cell of length 1. (The pattern
is repeated for as many qubits as needed.)\label{fig:troterized-circuits}}

\end{figure}

where $\Delta t$ is given in units of $\nicefrac{1}{J}$. The paper
\cite{barrattParallelQuantumSimulation2021} claims that due to the
translational invariance the error of the third circuit is quite low
as well. If $\hat{W}_{odd}$ is applied on q2 and q1 then they get
entangled. But since the translationally invariant iMPS with correct
initialization guaranties, that the relation between q1 and q2 is
the same as between q2 and q3, the effect of $W_{even}$ is indirectly
applied as well. Since $\hat{W}_{odd}$ has to take over the time
evolution for both $\hat{W}_{odd}$ and $\hat{W}_{even}$, it needs
to be squared, or alternatively a factor 2 in the exponent is needed.

I use this method for my simulations as well. Note however, that due
to the symmetry braking, the scalar product optimized for the time
evolution can not reach 1 anymore. The state evolved with this method
has a translational invariance with unit cell length 2 and cannot
equal the bra-MPS that is optimized in the time evolution.

\section{Final time evolution circuits}

\begin{figure}[H]
\begin{subfigure}[h]{0.6\linewidth}\centering

\includegraphics[viewport=0bp 0bp 525bp 404bp,clip,width=1\textwidth]{figures/results/other_figures/LEEL_with_W}

\end{subfigure} \hfill \begin{subfigure}[h]{0.4\linewidth}

\includegraphics[viewport=0bp 0bp 339bp 219bp,clip,width=1\textwidth]{figures/results/other_figures/LL_dagger}

\end{subfigure} \hfill \begin{subfigure}[h]{0.6\linewidth}\hfill

\includegraphics[viewport=0bp 0bp 525bp 312bp,clip,width=1\textwidth]{figures/results/other_figures/LEL_with_W}

\end{subfigure}

\caption{Left and middle: Cut out of the circuits implementing troterization
of 1st and 2nd order, respectively. Right: Circuit used in the thesis.
\cite{barrattParallelQuantumSimulation2021} claims that the error
is \textquotedblleft reduced somewhat\textquotedblright{} because of
the translational invariance with unit cell of length 1. (The pattern
is repeated for as many qubits as needed.)\label{fig:final-time-evolution-circuits}}
\end{figure}


\section{Parametrization of operators\label{sec:appendix:Parametrization-of-operators}}

\begin{figure}[H]
\begin{subfigure}[h]{0.5\linewidth}\centering

\includegraphics[viewport=20bp 0bp 360bp 126bp,clip,width=1\textwidth]{figures/results/other_figures/R_circuit_from_barratt-paper}

\end{subfigure} \hfill \begin{subfigure}[h]{0.5\linewidth}

\includegraphics[viewport=0bp 0bp 339bp 219bp,clip,width=1\textwidth]{figures/results/other_figures/R_circuit}

\end{subfigure}

\caption{Left: circuit for $\hat{R}$ in supplementary material figure 6 of
\autocite{barrattParallelQuantumSimulation2021}. Note that in their
convention the order of the qubits is reversed. Right: The circuit
used for this thesis.\label{fig:R-circuits}}
\end{figure}

\ref{fig:R-circuits} shows first the circuit for $\hat{R}$ from
the paper \autocite{barrattParallelQuantumSimulation2021}. The important
change to the circuit used here is that the qubits for the $c\hat{X}$
gate are reversed. This was done because the $\hat{X}$ gate on q0
commutes with $c\hat{X}$ if the target gate is q0. Therefore the
two $\hat{R}_{x}$ on q0 cancel. We could just as well remove them.
Therefore it was assumed, that the target gate of $c\hat{X}$ was
supposed to be q1.

\chapter{Results for Transverse Field Ising Model}

\begin{figure}
\makebox[1\textwidth][c]{

\includegraphics[width=0.55\textwidth]{figures/results/find_env_on_real_qc/rotosolve-optimization_limited-shot-number:_100__init-seed:_\lyxdot \lyxdot \lyxdot __max_iter_trace}

}\\ \makebox[1\textwidth][c]{

\includegraphics[width=0.55\textwidth]{figures/results/find_env_on_real_qc/rotosolve-optimization_limited-shot-number:_10000__init-seed:_\lyxdot \lyxdot \lyxdot __max_iter_trace}\includegraphics[width=0.55\textwidth]{figures/results/find_env_on_real_qc/rotosolve-optimization_limited-shot-number:_1000000__init-seed:_\lyxdot \lyxdot \lyxdot __max_iter_trace}

}\caption{Doubly sinusoidal optimizations stopping after 25 iterations for different
shot numbers. We see that the optimization tends to get nearer to
zero than $N^{-\nicefrac{1}{2}}$, presumably because the 6 measurements
equalize the error further, but not better than $N^{-1}$.\label{fig:Doubly-sinusoidal-optimizations-max-iter}}

\end{figure}

\end{document}
