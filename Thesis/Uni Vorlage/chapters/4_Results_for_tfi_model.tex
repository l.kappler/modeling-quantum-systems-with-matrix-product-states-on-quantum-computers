%% LyX 2.3.6 created this file.  For more info, see http://www.lyx.org/.
%% Do not edit unless you really know what you are doing.
\documentclass[english]{article}
\renewcommand{\familydefault}{\sfdefault}
\usepackage[utf8]{inputenc}
\usepackage[a4paper]{geometry}
\geometry{verbose}
\setlength{\parskip}{\medskipamount}
\setlength{\parindent}{0pt}
\usepackage{color}
\usepackage{babel}
\usepackage{float}
\usepackage{units}
\usepackage{amsmath}
\usepackage{graphicx}
\usepackage[unicode=true,
 bookmarks=false,
 breaklinks=false,pdfborder={0 0 1},backref=false,colorlinks=true]
 {hyperref}
\hypersetup{
 allcolors=blue}

\makeatletter

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% LyX specific LaTeX commands.
%% A simple dot to overcome graphicx limitations
\newcommand{\lyxdot}{.}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% User specified LaTeX commands.
\include{preamble_for_lyx_files}

\makeatother

\usepackage[bibstyle=authoryear,style=alphabetic]{biblatex}
\addbibresource{literatur.bib}
\begin{document}
\input{macros_for_lyx.tex}

In this chapter the algorithm was tested using the transverse field
Ising model:

\begin{gather}
\mathcal{H}_{\lambda}=J\sum_{i}\left(\sigma_{i}^{z}\sigma_{i+1}^{z}+\lambda\sigma_{i}^{x}\right)\:,\label{eq:hamiltonian}
\end{gather}

where $J$ is a factor with energy unit. All times will be given in
units of $\nicefrac{1}{J}$. This includes two runs of finding the
environment $\hat{V}$ on a real quantum computer. The time evolution
was tested by comparing the scalar product of the initial state and
the time evolved state, which is also known as Loschmidt Echo, with
the analytically calculated values generated by the code from \autocite{barrattParallelQuantumSimulation2021}.

\section{Finding the Ground State\label{sec:Finding-the-Ground-state}}

The initial state of the time evolution is set to the ground state
of the Hamiltonian in \ref{eq:hamiltonian} with $\lambda=1$. This
ground state was found by minimizing the expectation value of $\mathcal{H}$
with respect to the parameters of $\hat{U}$. As discussed in \ref{subsec:calculating-expectation-values.},
to do this, we need to find the environment $\hat{V}$ for every expectation
value. It is found using doubly sinusoidal rotosolve. Since this inner
optimization depends contains some randomness, the computed expectation
value has some noise. Therefore the method Nelder-Mead was used. The
optimization stopped after reaching 10000 iterations. The resulting
ground state Energy is approximately $-1.27\,J$ which coincides with
the analytical Energy given in supplementary Fig. 7.a of \autocite{barrattParallelQuantumSimulation2021}.

\section{Finding Parameters of the Environment $\hat{V}$ on a real Quantum
Computer\label{sec:Finding-Parameters-of-the-environment}}

As described in \ref{sec:Translationally-invariant-iMPS} the algorithm
for finding the environment $\hat{V}$ minimizes cost functions that
can be evaluated on a quantum computer. In order to know what to expect
and hope for let's look at optimal noise free simulations of the quantum
circuits first. Remember that even a noise free quantum computer has
an error in the estimated probabilities given by\textrm{ $\sigma_{i}=\sqrt{\frac{p_{i}(1-p_{i})}{N}}$}
as discussed in \ref{sec:optimization-algorithms}. The cost function
is evaluated using the following methods:
\begin{enumerate}
\item Exact simulations of the circuits returning the exact probabilities
with varying initial parameter.
\item Noise free simulation of circuits returning estimate of probability
assuming $N=10^{i}$ shots with $i\in\left\{ 2,4,6\right\} $ and
varying initial parameter.
\item Noise free simulation of circuits returning estimate of probability
assuming $N=10^{i}$ shots with $i\in\left\{ 2,4,6\right\} $ with
the same initial parameter as for the quantum computer.
\end{enumerate}
For the simulation of $N$ shots, Qiskit draws the returned estimate
from a normal distribution with the given standard deviation and the
exact probabilities as mean. Each simulation was repeated 10 times
with different seeds for the random initial parameter in simulation
types (1.) and (2.) and different seeds for the probability estimates
for simulations (2.) and (3.). $\hat{V}$ has three parameter. Therefore
every iteration has three steps each changing one parameter at a time.
After each parameter change, the new value of the cost function is
estimated/computed. They are shown as lines in \ref{fig:Doubly-sinusoidal-optimizations}.
The $\times$ markers show the exact traces. The optimization stops
once these values reach 0 plus a tolerance. The tolerance is $10^{-10}$
for the exact simulation. We see, that this optimization converges
quite rapidly and accurately. The tolerance is $10^{-4}$ for simulation
with limited shots. We see as expected, that for $N=10^{2}$ the optimization
doesn't get better after reaching $\sim0.1$. When the optimization
ends, is quite random. There even is a run (cyan) that already stopped
after optimizing just one parameter because the estimated trace happened
to be below 0.

\begin{figure}[H]
\makebox[1\textwidth][c]{

\includegraphics[width=0.6\textwidth]{figures/results/find_env_on_real_qc/rotosolve_optimization_simulating_exactly_\lyxdot \lyxdot \lyxdot _trace}\includegraphics[width=0.6\textwidth]{figures/results/find_env_on_real_qc/rotosolve-optimization_limited-shot-number:_100__init-seed:_\lyxdot \lyxdot \lyxdot _trace}\\
}\\ \makebox[1\textwidth][c]{

\includegraphics[width=0.6\textwidth]{figures/results/find_env_on_real_qc/rotosolve-optimization_limited-shot-number:_10000__init-seed:_\lyxdot \lyxdot \lyxdot _trace}\includegraphics[width=0.6\textwidth]{figures/results/find_env_on_real_qc/rotosolve-optimization_limited-shot-number:_1000000__init-seed:_\lyxdot \lyxdot \lyxdot _trace}

}\caption{Doubly sinusoidal optimizations for 10 different random initializations
shown in different colors. Exact traces are given by $\times$. The
optimization is stopping after the cost function value reaches $10^{-10}$
for the exact simulation in the top left and $10^{-4}$ for the others,
because 0 is the desired minimum. We see that rotosolve with exact
traces converges rapidly. For optimizations with estimated trace,
we see after the optimization stopped, that the exact traces for the
larger two $N$ are mainly in the range $(0,N^{-\nicefrac{1}{2}})$.
This is improved a little bit, when letting the algorithm run longer
as can be seen in \ref{fig:Doubly-sinusoidal-optimizations-max-iter}
in the Appendix. \label{fig:Doubly-sinusoidal-optimizations}}
\end{figure}

The optimizations with the initial parameters, that where also used
on the quantum computer, are shown in \ref{fig:Doubly-sinusoidal-optimizations-params-const}.
We see that the speed of convergence and range of exact traces after
the optimization are roughly the same as in \ref{fig:Doubly-sinusoidal-optimizations}.
Therefore the convergence doesn't seem to depend too much on the parameters.
This is practial, because since 26th of September, IBM limits free
access to its Quantum computers to 10 minutes, which limits the number
of parameter we can test. Estimating one trace with $N=10^{4}$ shots
took about half a minute on the quantum computer ibm\_lagos and ibm\_perth.
This was enough for three iteration steps in total, which are displayed
in \ref{fig:Doubly-sinusoidal-optimizations-on-qc}. Therefore the
error of the estimated probabilities would be roughly $\sigma\approx0.01$
if there was no noise. The first run with ibm\_perth made one optimization
step and then stopped because the estimated trace was already below
0. The second run made two optimization steps after which my usage
limit was reached. Note that this is not a full Iteration. only the
first two of three parameter where optimized. We can see that the
trace can be minimized to .3 without error correction. These estimations
only needed up to 6 qubits. Newer quantum computer with more qubits
that would allow error correction codes would probably give better
results. That this took 10 minutes while my laptop took less time
for all the simulations of this section is not surprising, since the
advantage for Quantum computers can only be expected when using a
many more qubits.

\begin{figure}[H]
\makebox[1\textwidth][c]{

\includegraphics[width=0.6\textwidth]{figures/results/find_env_on_real_qc/rotosolve-optimization_limited-shot-number:_100__simulation-seed:_\lyxdot \lyxdot \lyxdot _trace}\\
}\\ \makebox[1\textwidth][c]{

\includegraphics[width=0.6\textwidth]{figures/results/find_env_on_real_qc/rotosolve-optimization_limited-shot-number:_10000__simulation-seed:_\lyxdot \lyxdot \lyxdot _trace}\includegraphics[width=0.6\textwidth]{figures/results/find_env_on_real_qc/rotosolve-optimization_limited-shot-number:_1000000__simulation-seed:_\lyxdot \lyxdot \lyxdot _trace}

}\caption{10 doubly sinusoidal optimizations for the same initialization parameter
as those used on the quantum computer. The colors correspond to different
seeds for the artificial error of the estimated probability. The optimization
is stopping after the cost function value reaches $10^{-4}$. We see,
that the plots look qualitatively similar as in \ref{fig:Doubly-sinusoidal-optimizations}
and thus, that the convergence does not depend much on these parameters
but rather on the noise of the cost function.\label{fig:Doubly-sinusoidal-optimizations-params-const}}
\end{figure}

\begin{figure}[H]
\makebox[1\textwidth][c]{

\includegraphics[width=0.6\textwidth]{figures/results/find_env_on_real_qc/rotosolve_optimization_ibm_quantum-computers_trace}

}\caption{Two doubly sinusoidal optimization runs for the same initialization
on two quantum computers. The optimization would stop after the estimated
cost function gets below $10^{-2}$. The run on ibm\_lagos ended earlier
because the time limit for free usage was reached. Note that the y-axis
is not in log-scale this time. (Otherwise the second point of the
run on ibm\_perth could not be seen.)\label{fig:Doubly-sinusoidal-optimizations-on-qc}}
\end{figure}


\section{Time evolution\label{sec:Time-evolution-tests}}

The initial state for the time evolution was found in \ref{sec:Finding-the-Ground-state}.
It is the ground state for $\mathcal{H}_{1}$ from \ref{eq:hamiltonian}.
The time evolution uses the Hamiltonian $\mathcal{H}_{0.2}$, i.e.
$\lambda=0.2$. Changing a parameter of the Hamiltonian in this way
is known as a quantum quench. The Loschmidt echo of this system shows
noticeable peaks, first discovered in \autocite{heylDynamicalQuantumPhase2013}.
The exact results were calculated using the code from \autocite{barrattParallelQuantumSimulation2021}
for comparison. Note however that the algorithm described in that
paper on which this work is based, didn't reproduce the exact results
either, as can be seen in \ref{fig:Loschmidt-echo-1} on the right.

The following simulations show results from the algorithm for several
hyper parameters discussed in \ref{subsec:Hyper-parameter-of-algorithm}.
Furthermore most simulations used tensor networks for the cost functions
even if they can be implemented as quantum circuits. This works faster
than simulating the circuits. It was checked, that the tensor networks
that can be implemented as a circuit yield the same results as the
circuits. Using tensor networks also allows us to test the algorithm
with and without the approximations needed to run the tensor network
equivalent on the quantum computer. First let's look at simulations
that reduce the approximations introduced within the algorithm to
a minimum. The algorithm worked by maximizing the local scalar product
between the time evolved previous state and the variationally optimized
state. Since this local scalar product is defined as the largest norm
of the eigenvalues of the transfer matrix, it would reduce many error
sources to compute that eigenvalue directly. We can compute the transfer
matrix by contracting the tensor network corresponding to \ref{fig:transfer-matrix-squared-with-time-evolution}
and use the well established method 'eigvals' from scipy.linalg to
compute the eigenvalues. Taking the absolute value and returning the
largest of them gives the wanted local scalar product.

\begin{figure}

\includegraphics[width=0.5\textwidth]{\string"figures/results/third_hamiltonian_with_quench__eigenvalue^1_overlap__classically__,g=1-.2_with_zz:1__dt=.01_BFGS,comparison of time evolution with classical eigenvalue calculation_LoschmidtEcho\string".pdf}\includegraphics[viewport=0bp 0bp 452bp 295bp,clip,width=0.5\textwidth]{\string"figures/external/time evolution paper DOI: 10.1038_slash_s41534-021-00420-3/Fig4\string".pdf}\caption{\label{fig:Loschmidt-echo-1}Left: Loschmidt echo of the time evolution.
The evolution was using $\Delta t=0.01$ and the eigenvalue function
'eigvals' from scipy to get the largest eigenvalue norm of the transfer
matrix. The blue and orange curves only use different optimization
methods and powers of the transfer matrix. The algorithm is stable
since the orange and blue curve are on top of each other. $\hat{U}$
was parameterized by 3 repetitions of the circuit in the middle of
\ref{fig:circuits-of-parametrized-operators}, and therefore by 12
parameter. Right: Graph taken from \autocite{barrattParallelQuantumSimulation2021}
for the same system and same parametrization of $\hat{U}$. The blue
and orange curve on the left and the green on the right should have
matched.}
\end{figure}

The blue and orange graphs in \ref{fig:Loschmidt-echo-1} on the left
are simulations, where only the optimization method and the power
of the transfer matrix used, differ. The black dashed line is the
exact result for the Loschmidt echo using the code from \autocite{barrattParallelQuantumSimulation2021}.
The graph on the right is from \autocite{barrattParallelQuantumSimulation2021}
and should show the same exact curve in black since the code used
to generate it should be the same in both cases. However there the
first peak seems to be a little bit higher than 0.5 which it is not
on the left. The parameters needed to generate that code are the two
values for $\lambda$ which were given in the paper as $1$ for the
ground state and $0.2$ for the time evolution as well. The green
curve of that paper was created with the algorithm described in \ref{chap:impl_on_qc}
except for the differences explicitly named there. I was not able
to find out what optimization algorithm they used. Their code uses
Nelder-Mead and BFGS in several places. However the parameter for
$\lambda$ in the code did not match those given in the paper, nor
did the exact graph with the parameters from the code match the graph
from the paper. The parametrization of $\hat{U}$ is the same for
the blue and orange curve on the left and the green on the right.
Therefore they should be the same or rather due to my changes to the
algorithm, the orange and blue curve should have been more accurate.
There are small peaks in the correct frequency, i.e. they occur at
the time they should. But there also seems to be a different larger
wave with a higher frequency. This might be due to some bug in the
code or because the convergence of the ground state was not accurate
enough. The latter is improbable because some of the following simulations
that should theoretically be less accurate are in fact better.

The next simulation minimized $\frac{|LE-L|^{2}}{|L|^{2}}$ to find
$L$ and the eigenvalue of the transfer matrix as described in \ref{subsec:Estimating-local-Scalar-products}.
The approximation $\Re(LEL^{\dagger})\approx|LEL^{\dagger}|$ was
not yet used. For all the following simulations the power of the transfer
matrix used is 1. In \ref{fig:Loschmidt-echo-simulate-l-two-delta-t}
on the left BFGS was used for optimization. On the right Nelder-Mead.
The simulation on the left with $\Delta t=0.01$ and BFGS performed
poorly because the system jumped into an eigenstate of the new Hamiltonian
$\mathcal{H}_{0.2}$ right after the first peak and therefore stopped
changing. The simulation with $\Delta t=0.01$ performed the best
from all those shown. The peaks are still smaller than those from
\ref{fig:Loschmidt-echo-1} on the right. This might be because the
exact Loschmidt echo computation might assume a different logarithm
than the natural log. However, replacing it by $\log_{2}$ did not
lead to a match. Furthermore, the frequency of the peaks is too high,
but at least hey are clearly visible. The results for $\Delta t=0.01$
match those for the other $\Delta t$ right up to the iteration where
the state collapsed into an eigenstate. Unfortunately there was no
time to repeat the simulation for $\Delta t=0.01$. The simulation
on the right are also better than \ref{fig:Loschmidt-echo-1} but
not as good as the time evolution with BFGS and the smaller $\Delta t$.

\begin{figure}
\centering

\includegraphics[width=0.5\textwidth]{\string"figures/results/third_hamiltonian_with_quench__eigenvalue^1_overlap__optimizing_l_tn__,g=1-.2_with_zz:1__dt=.001_BFGS,comparison of time evolution for two different time steps_LoschmidtEcho\string".pdf}\includegraphics[width=0.5\textwidth]{\string"figures/results/third_hamiltonian_with_quench__eigenvalue^1_overlap__optimizing_l_tn__,g=1-.2_with_zz:1__dt=.01_Nelder-Mead,comparison of time evolution with analytical results_LoschmidtEcho\string".pdf}\caption{Left: Blue and orange: Loschmidt echo of the time evolution using
BFGS and optimization of $L$ via the exact cost function, only difference
is the given $\Delta t$. Black is the analytical value from earlier.
Right: Loschmidt echo of time evolution using Nelder-Mead, $\Delta t=0.01$
and optimization of $L$ via exact cost function,\label{fig:Loschmidt-echo-simulate-l-two-delta-t}}
\end{figure}

Next should follow the simulation with the approximation $\Re(LEL^{\dagger})\approx|LEL^{\dagger}|$
discussed in \ref{subsec:Estimating-local-Scalar-products}. We don't
expect major differences to the provided plots, however there was
no time to perform the tests. It is necessary to perform them in future
work, to ensure that results from a quantum computer could in theory
be trusted.
\end{document}
