%% LyX 2.3.6 created this file.  For more info, see http://www.lyx.org/.
%% Do not edit unless you really know what you are doing.
\documentclass[english]{article}
\renewcommand{\familydefault}{\sfdefault}
\usepackage[utf8]{inputenc}
\usepackage[a4paper]{geometry}
\geometry{verbose}
\setlength{\parskip}{\medskipamount}
\setlength{\parindent}{0pt}
\usepackage{color}
\usepackage{babel}
\usepackage{amsmath}
\usepackage[unicode=true,
 bookmarks=false,
 breaklinks=false,pdfborder={0 0 1},backref=false,colorlinks=true]
 {hyperref}
\hypersetup{
 allcolors=blue}

\makeatletter
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% User specified LaTeX commands.
\include{preamble_for_lyx_files}

\makeatother

\usepackage[bibstyle=authoryear,style=alphabetic]{biblatex}
\addbibresource{literatur.bib}
\begin{document}
\input{macros_for_lyx.tex}


\section{Summary}

This thesis explained how Quantum Computers could be used to simulate
quantum system using methods based on Matrix Product States (MPS).
To do this a set of rules and methods was developed for translating
tensor networks into quantum circuits and vice versa.

In general one can say that a estimated expectation value from repeated
measurements will even without noise never reach machine accuracy
because the number of repeated measurements needed scales exponentially
with the required number of accurate digits, see \ref{sec:optimization-algorithms}.
However, for large systems machine accuracy isn't possible on a classical
computer either because the state needs to be approximated to fit
into memory. In such cases it might be possible to get more accurate
results using a quantum computer.

The described algorithm has the potential to represent the same approximated
states as the MPS with same bond dimension, while storing fewer parameters,
see \ref{subsec:Parametrization-of-used-operators}, and with fewer
computational operations.

Since I only had access via the free plan to IBM's quantum computers,
it was not possible to test the algorithm extensively. The only performed
test used 7-qubit quantum computers. It showed that this quantum computer
was still too noisy and slow to have an advantage. This is not surprising
since quantum supremacy can only be possible with at least 1000 times
as many qubits.

The algorithm was tested by comparing the Loschmidt echo of the transversal
field Ising model of the simulation with exact values. The simulations
performed showed that the results have qualitative similarities but
are still very far off from the exact simulations. Furthermore the
algorithm is quite unstable. For example in some cases the time evolved
state suddenly jumped into an eigenstate of the Hamiltonian. It is
however possible to detect if the simulation won't yield good results
by checking if the scalar product between the previous state after
a time evolution and the optimized state approximating this time evolution
has norm near 1. This value has to be calculated for the evolution
anyway. There are many possible reasons for the bad accordance between
exact results and the time evolution of the algorithm as well as some
solutions that future research could implement, some of them are listed
in the following section.

\section{Outlook}

We showed in section \ref{sec:Finding-Parameters-of-the-environment}
that density matrices can be compared faithfully by the procedure
described in \ref{subsec:Example:-trace-of-density-matrices}. The
next step is to show that the algorithm for time evolution will work.
To do so, tests need to be performed with the approximations from
\ref{subsec:Estimating-local-Scalar-products}. Without this approximation
for this tensor network for time evolution, it cannot be translated
to quantum computers. Furthermore there are many possibilities to
improve the algorithm to get more accurate results:

\subsection{Problems of the method and improvement possibilities\label{subsec:Problems-of-the-method-and-improvement-possibilities}}

\subsubsection{Algorithm}
\begin{enumerate}
\item All simulations where preformed using a bond order of 2 for the MPS.
If the correct time evolved state is not within the manifold of representable
states the result cannot be accurate. Increasing this size could improve
the accuracy a lot. There are also other methods to initialize a system
such as a Brick wall circuit \autocite{linRealImaginaryTimeEvolution2021}.
There the matrices are not in a oblique line but rather like a brick
wall stacked over and next to each other. This has the advantage,
that more qubits can be evolved simultaneously which could reduce
the execution time and noise of the result when actually running on
a quantum computer.
\item Optimizations using algorithms such as BFGS, Nelder-Mead and doubly
rotosolve depend on the initialization parameter and can generally
be quite unstable, i.e. find only a local minimum etc. Therefore one
should check if the final cost function value of the time evolution
or for finding the (mixed) environment are optimal enough and, if
not, restart the optimization. Doing this might reduce the probability
of the state collapsing into the eigenstate of the Hamiltonian such
as in \ref{fig:Loschmidt-echo-simulate-l-two-delta-t}.
\item The implementation of the algorithm assumed non-degeneracy for the
transfer matrix and similarly for the environment $\hat{V}$. The
implementation should check, if this is the case and warn when it
is not. For example, to check if this is true the parameter for $\hat{U}$
should be optimized several times with different starting parameter.
Then calculate the scalar product between these time evolved MPS.
If their norm is one, the method worked, otherwise the transfer matrix
is degenerate. Note that it is not always possible to compare the
values of $\hat{U}$ because several parameter combinations could
represent the same state.
\end{enumerate}

\subsubsection{When running on a quantum computer}

We saw with the few runs on the quantum computer and the simulations,
that the optimization with rotosolve depends heavily on the number
of shots used for the estimation. However, the first steps of rotosolve
perform well, even if the shot number is not very high. Therefore
it should be possible to start the optimization with just a few shots
to get to roughly the correct position and increase the shot number
the nearer the cost function is to 0.

For every time step there are many estimations of certain values.
Since even noise free quantum computer estimations would have a statistical
error of $\sqrt{N(p-p^{2})}$, every estimation introduces a small
error. Due to the iterative nature of the algorithm these would add
up quickly and could become quite large. Therefore it would be good
to reduce the number of measurements needed to get to a certain point
in time. For this, larger $\Delta t$ are needed while preserving
accuracy. Therefore higher order approximations of the time evolution
operator are needed.

\subsection{Possible debug strategies for this implementation}

The most visible problem in the results is the instability of the
optimizations. Besides the error sources from \ref{subsec:Problems-of-the-method-and-improvement-possibilities}
and possible remaining bugs in the code, there are other possible
reasons for the visible difference in the Loschmidt echo in \ref{sec:Time-evolution-tests}:
Possibly the optimization to obtain the ground state did not converge
correctly. The ground state used should be compared with the ground
state computed with classical methods.

Furthermore, sources of error from optimization routines could be
eliminated by computing the Loschmidt echo as the scalar product of
the original state on both sides but with the time evolution operator
to the power of time step in between. Note that this will not yield
the exact results that we would hope to get in the optimization. The
time evolution operator was defined in \ref{subsec:Troterization-appendix}
and does not preserve the translational invariance. However, in each
iterative time evolution step, this evolved state was approximated
with a translation-invariant state, which preserved the invariance.
This would not be the case when computing the Loschmidt echo without
optimization. However, it may still be helpful to compute the Loschmidt
echo in this way for debugging purposes.\\

In conclusion, this work shows that it should be possible to simulate
a quantum system with MPS using the described algorithm. To obtain
quantitatively correct results from circuit simulations, the instability
of the optimization algorithms and possibly degenerate transfer matrices
must be reduced, codes for larger bond orders must be developed, and
the approximation of the time evolution must be improved. In this
sense there still is a lot to do but one can be hopeful that it will
work out in the end.
\end{document}
