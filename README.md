# Modeling quantum systems with Matrix Product States on quantum computers (Bachelor's thesis)

**Abstract**

Quantum computers have the potential to simulate quantum systems faster and more accurately than their classical counterparts. Based on existing literature, this work discusses and uses a set of rules and methods for translating matrix product states (MPS) and other tensor network methods into quantum circuits. We apply these methods to approximate and simulate the ground state of the transversal field Ising model with MPS. The Loschmidt echoes obtained from time evolution are compared to exact analytical values. The simulations show qualitative agreement of the main features with the analytical solution, however quantitative accuracy and stability need further improvements. Suggestions for future development are proposed in the conclusion.



# Directories

- `code_for_thesis` contains all the code used in the thesis.
- `Einarbeitung` contains the documents of the introductory report.
- `git_hooks` contains pre-commit hooks used to ensure that the thesis pdf and the list of installed packages for the python code (see ReadMe in `code_for_thesis`) are always up to date.
- `kleine_Programmieraufgaben` contains small codes for different example exercises I did as an introduction to the field.
- `Thesis` contains all the files used to create the final pdf.

all the code had only been tested for Linux/Ubuntu 22.04 and Python 3.11.5. For the versions of the libraries see the ReadMe in `code_for_thesis`.
